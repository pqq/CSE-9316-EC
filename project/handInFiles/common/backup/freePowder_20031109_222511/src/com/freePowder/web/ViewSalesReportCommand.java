/*
package com.freePowder.web;

import java.io.*;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.freePowder.beans.NewsBean;
import com.freePowder.web.helper.DelegateFactory;
import com.freePowder.web.helper.AdminDelegate;
import com.freePowder.business.AdminServiceException;

import com.freePowder.utility.Debug;


public class viewSalesReportCommand implements Command
{
	private static AdminDelegate adminDelegate;
	private static Debug d;
	
	public ViewSalesReportCommand()
	{
		d = new Debug("ViewSalesReportCommand()");
		d.print("Constructor()");
		adminDelegate = DelegateFactory.getInstance().getAdminDelegate();
	}
	
	public String execute(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException
	{
		d.print("execute()");
		
		HttpSession session = request.getSession();
		
		try
		{
			List list = adminDelegate.getSalesLog();
			request.setAttribute("newslist", list);
						
			//return "/testnews.jsp";
			return "/index.jsp";
		}
		catch (AdminServiceException e)
		{
			e.printStackTrace();
		}	
		return "/index.jsp";
	}
}
*/