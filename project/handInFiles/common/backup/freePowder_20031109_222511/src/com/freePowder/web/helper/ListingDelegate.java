/*
 * file:	ListingDelegate.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	08. Nov 2003
 *
 */
package com.freePowder.web.helper;

import java.util.List;

import com.freePowder.beans.NewsBean;
import com.freePowder.business.ListingService;
import com.freePowder.business.ListingServiceException;

import com.freePowder.utility.Debug;


public abstract class ListingDelegate {

	protected abstract ListingService getListingService();
	protected Debug d;

	public List getNews() throws ListingServiceException {
		d = new Debug("ListingDelegate()");
		d.print("getNews()");
		return getListingService().getNews();
	}
	
	public List getWeather() throws ListingServiceException {
		d = new Debug("ListingDelegate()");
		d.print("getWeather()");
		return getListingService().getWeather();
	}

}
