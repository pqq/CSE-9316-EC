/*
 * file:	AdminService.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	08. Nov 2003
 *
 */

package com.freePowder.business;

import java.util.List;

import com.freePowder.beans.SupplierBean;
import com.freePowder.beans.SalesLogBean;


public interface AdminService
{
	List getSuppliers() throws AdminServiceException;
	SupplierBean getSupplier(int supplier_pk) throws AdminServiceException;
	boolean updateDatabase(String xml_url) throws AdminServiceException;
}
