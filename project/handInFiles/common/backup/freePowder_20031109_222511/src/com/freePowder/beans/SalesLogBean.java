/**
 * Class that represents news for the system
 *
 * @author  $Nosh
 */

package com.freePowder.beans;

import java.io.Serializable; 

public class SalesLogBean implements Serializable
{
	private int slslpk;
	private int prdpk;
	private String brandname;
	private String productname;
	private int usr_pk;
	private int inprice;
	private int outprice;
	private int noproducts;
	private String orderdate;
	private String shippingdate;
	private String shippingaddress;
	private String shippingstatus;
	
	public int getSlslPk()
	{
		return slslpk;
	}
	public void setSlslPk(int slslpk)
	{
		this.slslpk= slslpk;
	}
	public int getProductPk()
	{
		return prdpk;
	}
	public void setProductPk(int prdpk)
	{
		this.prdpk= prdpk;
	}
	public String getBrandName()
	{
		return brandname;
	}
	public void setBrandName(String brandname)
	{
		this.brandname = brandname;
	}
	public String getProductName()
	{
		return productname;
	}
	public void setProductName(String productname)
	{
		this.productname = productname;
	}
	public int getUsrPk()
	{
		return usr_pk;
	}
	public void setUsrPk(int usr_pk)
	{
		this.usr_pk=usr_pk;
	}
	public int getInPrice()
	{
		return inprice;
	}
	public void setInPrice(int inprice)
	{
		this.inprice = inprice;
	}
	public int getOutPrice()
	{
		return outprice;
	}
	public void setOutPrice(int outprice)
	{
		this.outprice = outprice;
	}
	public int getNoProducts()
	{
		return noproducts;
	}
	public void setNoProducts(int noproducts)
	{
		this.noproducts = noproducts;
	}
	public String getOrderDate()
	{
		return orderdate;
	}
	public void setOrderDate(String orderdate)
	{
		this.orderdate = orderdate;
	}
	public String getShippingDate()
	{
		return shippingdate;
	}
	public void setShippingDate(String shippingdate) 
	{
		this.shippingdate = shippingdate;
	}
	public String getShippingAddress()
	{
		return shippingaddress;
	}
	public void setShippingAddress(String shippingaddress)
	{
		this.shippingaddress = shippingaddress;
	}
	public String getShippingStatus()
	{
		return shippingstatus;
	}
	public void setShippingStatus(String shippingstatus)
	{
		this.shippingstatus = shippingstatus;
	}
	public String toString()
	{
		return "[SALE LOG]" + prdpk +""+ brandname +""+ usr_pk +""+ inprice +""+ outprice +""+noproducts +""+ orderdate+""+shippingdate +""+ shippingstatus ;
	}
}
