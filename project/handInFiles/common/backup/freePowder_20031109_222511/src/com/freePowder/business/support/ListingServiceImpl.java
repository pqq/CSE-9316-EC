/*
 * file:	ListingServiceImpl.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	07. Nov 2003
 *
 */

package com.freePowder.business.support;

import java.util.List;

import com.freePowder.beans.NewsBean;
import com.freePowder.beans.WeatherBean;
import com.freePowder.business.ListingService;
import com.freePowder.business.ListingServiceException;
import com.freePowder.dao.DAOFactory;
import com.freePowder.dao.DataAccessException;
import com.freePowder.dao.NewsDAO;
import com.freePowder.dao.WeatherDAO;

import com.freePowder.utility.Debug;


public class ListingServiceImpl implements ListingService {


	private NewsDAO newsDao;
	private WeatherDAO weatherDao;
	private Debug d;

	public ListingServiceImpl() {
		super();
		newsDao    = DAOFactory.getInstance().getNewsDAO();
		weatherDao = DAOFactory.getInstance().getWeatherDAO();
		d = new Debug("ListingServiceImpl()");
	}

	public List getNews() throws ListingServiceException {
		try {
			return newsDao.getNews();
		} catch (DataAccessException e) {
			throw new ListingServiceException("Unable to find news", e);
		}
	}
	
	public List getWeather() throws ListingServiceException {
		d.print("getWeather()");
		try {
			return weatherDao.getWeather();
		} catch (DataAccessException e) {
			throw new ListingServiceException("Unable to find weather", e);
		}
	}



}
