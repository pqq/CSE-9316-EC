/*
 * file:	WeatherBean.java
 * author:	Lukasz Klenner (lukasz@onepenguin.net)
 * started:	09. Nov 2003
 *
 */

package com.freePowder.beans;

import java.io.Serializable;
import java.sql.*;

public class WeatherBean implements Serializable
{
	
	private String location;
	private String report;
	private int temp;
	private String myshort;
	private String timestamp;
	

	/** Creates a new instance of Weather */
	public WeatherBean(){
	}
	
	/** Get and set location */
	public String getLocation(){
		return location;
	}
	public void setLocation(String s){
		this.location = s;
	}
	
	/** Get and set report */
	public String getReport(){
		return report;
	}
	public void setReport(String s){
		this.report = s;
	}
	
	/** Get and set temp */
	public int getTemp(){
		return temp;
	}
	public void setTemp(int i){
		this.temp = i;
	}
	
	/** Get and set short */
	public String getShort(){
		return myshort;
	}
	public void setShort(String s){
		this.myshort = s;
	}
	
	public String getTimestamp(){
		return timestamp;
	}
	public void setTimestamp(java.sql.Date d){
		this.timestamp = d.toString();
	}
	
	
	public String toString(){
		return "This is a weather bean"; //+ hashCode();
	}
}
