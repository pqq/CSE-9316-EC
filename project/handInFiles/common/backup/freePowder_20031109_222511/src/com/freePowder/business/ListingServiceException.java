/*
 * file:	ListingServiceException.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	07. Nov 2003
 *
 */

package com.freePowder.business;

import com.freePowder.common.NestedException;



public class ListingServiceException extends NestedException {


	public ListingServiceException(String message) {
		super(message);
	}


	public ListingServiceException(String message, Throwable cause) {
		super(message, cause);
	}


	public ListingServiceException(Throwable cause) {
		super(cause);
	}

}
