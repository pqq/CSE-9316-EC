/*
 * file:	UserDelegate.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	02. Nov 2003
 *
 */
package com.freePowder.web.helper;

import java.util.List;

import com.freePowder.beans.UsrBean;
import com.freePowder.business.UserService;
import com.freePowder.business.UserLoginFailedException;
import com.freePowder.business.UserServiceException;

import com.freePowder.utility.Debug;


public abstract class UserDelegate {

	protected abstract UserService getUserService();
	protected Debug d;

	public UsrBean login(String loginname, String password) throws UserLoginFailedException {
		d.print("login");
		return getUserService().login(loginname, password);
	}
	
	public void addUser(UsrBean bean) throws UserServiceException {
		getUserService().addUser(bean);
		d.print("addUser");
	}


}
