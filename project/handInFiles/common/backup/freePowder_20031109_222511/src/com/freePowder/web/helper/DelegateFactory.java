/*
 * file:	DelegateFactory.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	02. Nov 2003
 *
 */

package com.freePowder.web.helper;

import java.util.HashMap;



public class DelegateFactory {

	private HashMap delegates = new HashMap();

	private static DelegateFactory instance = new DelegateFactory();

	public static DelegateFactory getInstance() {
		return instance;
	}
		
	private DelegateFactory() {
		delegates.put("User", UserDelegateImpl.getInstance());
		delegates.put("Listing", ListingDelegateImpl.getInstance());
		delegates.put("Admin", AdminDelegateImpl.getInstance());
	}

	public UserDelegate getUserDelegate() {
		return (UserDelegate) delegates.get("User");
	}

	public ListingDelegate getListingDelegate() {
		return (ListingDelegate) delegates.get("Listing");
	}

	public AdminDelegate getAdminDelegate() {
		return (AdminDelegate) delegates.get("Admin");
	}

}
