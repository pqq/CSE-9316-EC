/*
 * file:	UserLoginFailedException.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	02. Nov 2003
 *
 */

package com.freePowder.business;

import com.freePowder.common.NestedException;



public class UserLoginFailedException extends NestedException {


	public UserLoginFailedException(String message) {
		super(message);
	}


	public UserLoginFailedException(String message, Throwable cause) {
		super(message, cause);
	}

	public UserLoginFailedException(Throwable cause) {
		super(cause);
	}

}
