/*
 * file:	SalesLogDAOImpl.java
 * author:	...
 * started:	...
 *
 */


package com.freePowder.dao.support;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.*;

import com.freePowder.beans.SalesLogBean;
import com.freePowder.common.ServiceLocator;
import com.freePowder.common.ServiceLocatorException;
import com.freePowder.dao.DataAccessException;
import com.freePowder.dao.SalesLogDAO;

public class SalesLogDAOImpl implements SalesLogDAO
{
	
	private static ServiceLocator services;
	
	// Creates a new instance of SalesLogDAOImpl
	public SalesLogDAOImpl()
	{
		try
		{
			services = ServiceLocator.getInstance();
		}
		catch (ServiceLocatorException e)
		{
			e.printStackTrace();
		}
	}

	private SalesLogBean createSalesLogBean(ResultSet rs) throws SQLException
	{
		SalesLogBean saleslog = new SalesLogBean();

		saleslog.setSlslPk(rs.getInt("sales_log_pk"));
		saleslog.setProductPk(rs.getInt("product_pk"));
		saleslog.setBrandName(rs.getString("brandname"));
		saleslog.setProductName(rs.getString("productname"));
		saleslog.setUsrPk(rs.getInt("usr_pk"));
		saleslog.setInPrice(rs.getInt("in_price"));
		saleslog.setOutPrice(rs.getInt("out_price"));
		saleslog.setNoProducts(rs.getInt("no_items"));
		saleslog.setOrderDate(rs.getString("order_date"));
		saleslog.setShippingDate(rs.getString("shipping_date"));
		saleslog.setShippingAddress(rs.getString("shipping_address")); 
		saleslog.setShippingStatus(rs.getString("shipping_status"));
		return saleslog;

	}


	public List getSalesLog() throws DataAccessException
	{
		List list = new ArrayList();
		
		Connection con = null;
		try
		{
			con = services.getConnection();
			PreparedStatement stmt = con.prepareStatement("select * from sales_log");
		//	stmt.setString(1, bean.getLoginname());
		//	stmt.setString(2, bean.getPassword());
		//	stmt.setString(3, bean.getFirst_name());
		//	stmt.setString(4, bean.getLast_name());
		//	stmt.setString(5, bean.getEmail());
		//	stmt.setString(6, bean.getSex());
		//	stmt.setString(7, bean.getAddress());
		//	stmt.setString(8, bean.getZip());
			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				list.add(createSalesLogBean(rs));
			}
		}
		catch (ServiceLocatorException e)
		{
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		}
		catch (SQLException e)
		{
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		}
		finally
		{
			if (con != null)
			{
				try
				{
					con.close();
				}
				catch (SQLException e1)
				{
					e1.printStackTrace();
				}
			}
		}
		return list;
	}
}
 
