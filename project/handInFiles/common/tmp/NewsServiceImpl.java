/*
 * NewsServiceImpl.java Created on 12/08/2003
 * 
 */
package com.freePowder.business.support;

import java.util.List;

import com.freePowder.beans.NewsBean;
import com.freePowder.business.NewsService;
import com.freePowder.business.NewsServiceException;
import com.freePowder.dao.DAOFactory;
import com.freePowder.dao.DataAccessException;
import com.freePowder.dao.NewsDAO;

/**
 * Implementation of NewsService
 * @author Nosh
 */
public class NewsServiceImpl implements NewsService
{
	private NewsDAO newsDao;
	
	public NewsServiceImpl()
	{
		super();
		newsDao = DAOFactory.getInstance().getNewsDAO();
	}

	public NewsBean findByNewsPk(int newsId) throws NewsServiceException
	{	
		try
		{
			return newsDao.findByNewsPk(newsId);
		}
		catch (DataAccessException e)
		{
			throw new NewsServiceException("Unable to find news records", e);
		}
	}

	public void addNews(NewsBean bean) throws NewsServiceException
	{
		try {
			newsDao.insert(bean);
		}
		catch (DataAccessException e)
		{
			throw new NewsServiceException("Unable to add phone record", e);
		}
	}

	public NewsBean deleteNews(int id) throws NewsServiceException
	{
		try
		{
			NewsBean p = newsDao.findByNewsPk(id);
			newsDao.delete(id);
			return p;
		}
		catch (DataAccessException e)
		{
			throw new NewsServiceException("Unable to delete phone record", e);
		}
	}

	public List getNews() throws NewsServiceException
	{
		try
		{
			return newsDao.getNews();
		}
		catch (DataAccessException e)
		{
			throw new NewsServiceException("Unable to find record: ", e);
		}
	}

}
 
