/*
 * NewsServiceException.java Created on 12/08/2003
 *  
 */
package com.freePowder.business;

import com.freePowder.common.NestedException;

/**
 * @author Nosh
 */
public class NewsServiceException extends NestedException
{
	public NewsServiceException(String message)
	{
		super(message);
	}

	public NewsServiceException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public NewsServiceException(Throwable cause)
	{
		super(cause);
	}

}
 
