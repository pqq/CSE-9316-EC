/*
 * NewsService.java Created on 12/08/2003
 *  
 */
package com.freePowder.business;

import java.util.List;

import com.freePowder.beans.NewsBean;
//import com.freePowder.beans.UserBean;

/**
 * @author nosh
 */
public interface NewsService
{	

	
	void addNews(NewsBean bean) throws NewsServiceException;
	
	NewsBean deleteNews(int id) throws NewsServiceException;
	
	NewsBean findByNewsPk(int news_pk) throws NewsServiceException;
	
	List getNews() throws NewsServiceException;
}
 
