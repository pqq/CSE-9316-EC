<%@ page contentType="text/html" import="com.freePowder.beans.*,java.util.*, 
                                         com.freePowder.web.helper.DelegateFactory,
										 com.freePowder.web.helper.ListingDelegate,
										 import com.freePowder.business.ListingServiceException"%>
										 
<% ListingDelegate listingDelegate;
   ListingDelegate listingDelegate2;
   listingDelegate = DelegateFactory.getInstance().getListingDelegate();
   listingDelegate2 = DelegateFactory.getInstance().getListingDelegate();
   
   try {
   		List list1 = listingDelegate.getNews();
		request.setAttribute("newslist", list1);
   } 
   catch (ListingServiceException e) {
		e.printStackTrace();
   }
   
   try {
   		List list2 = listingDelegate2.getWeather();
		request.setAttribute("weatherlist", list2);
   } 
   catch (ListingServiceException e) {
		e.printStackTrace();
   }
%>
<!-- Start Home Section-->
<table width="100%"  border="0" cellspacing="5" cellpadding="0">
  <tr>
    <td width="80"><img src="images/spacer.gif" width="80" height="30"></td>
    <td valign="top">
	<!-- Start News Table -->
	<jsp:useBean id="entry" class="com.freePowder.beans.NewsBean"/>
	<jsp:useBean id="newslist" class="java.util.ArrayList" scope="request"/>
	<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#EBF1FB">
	  <tr>
		<td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td width="122"><img src="images/news_01.gif" width="122" height="80"></td>
			<td background="images/news_02.gif"><img src="images/news_02.gif" width="31" height="80"></td>
			<td width="247"><img src="images/news_03.jpg" width="247" height="80"></td>
		  </tr>
		</table></td>
	  </tr>
	  <tr>
		<td>
		  <table width="100%"  border="0" cellspacing="3" cellpadding="0">
			<% for (int i = 0; i < newslist.size(); i++) { 
			entry = (NewsBean) newslist.get(i); %>
			<tr>
			  <td bgcolor="#4A6077"><div class="newstitle"><jsp:getProperty name="entry" property="heading"/></div><div class="newsdate"><jsp:getProperty name="entry" property="timestamp"/></div></td>
			</tr>
			<tr>
			  <td bgcolor="#FFFFFF"><div class="newstext"><jsp:getProperty name="entry" property="body"/></div></td>
			</tr>
			<% } %>
		  </table></td>
	  </tr>
	</table>
	<!-- End News Table -->	
	</td>
    <td width="285" valign="top">
	<!-- Start Weather Table-->
	<jsp:useBean id="entry2" class="com.freePowder.beans.WeatherBean"/>
	<jsp:useBean id="weatherlist" class="java.util.ArrayList" scope="request"/>
	<table width="285" border="0" cellpadding="0" cellspacing="0" bgcolor="#EBF1FB">
      <tr>
        <td><img src="images/weather.gif" width="285" height="79"></td>
      </tr>
      <% for (int i = 0; i < weatherlist.size(); i++) { 
			entry2 = (WeatherBean) weatherlist.get(i); %>
	  <tr>
        <td><table width="100%"  border="0" cellspacing="3" cellpadding="0">
			<tr>
              <td bgcolor="#FFFFFF" class="weather"><b>Location: </b></td>
              <td bgcolor="#EBF1FB" class="weather"><jsp:getProperty name="entry2" property="location"/></td>
            </tr>
            <tr>
              <td bgcolor="#FFFFFF" class="weather"><b>Date:</b></td>
              <td bgcolor="#EBF1FB" class="weather"><jsp:getProperty name="entry2" property="timestamp"/></td>
            </tr>
            <tr>
              <td bgcolor="#FFFFFF" class="weather"><b>Temperature:</b></td>
              <td bgcolor="#EBF1FB" class="weather"><jsp:getProperty name="entry2" property="temp"/>&deg;c</td>
            </tr>
			<tr>
              <td bgcolor="#FFFFFF" class="weather"><b>Weather:</b></td>
              <td bgcolor="#EBF1FB" class="weather"><jsp:getProperty name="entry2" property="short"/></td>
            </tr>
            <tr>
              <td colspan="2" bgcolor="#FFFFFF" class="weather"><b>Report</b></td>
            </tr>
			<tr>
              <td colspan="2" bgcolor="#EBF1FB" class="weather"><jsp:getProperty name="entry2" property="report"/></td>
            </tr>			
        </table></td>
      </tr>
	  <tr>
	  	<td bgcolor="#F2BC02"><img src="images/spacer.gif" width="200" height="5"></td>
	  </tr>
	  <% } %>
    </table>
	<!-- End Weather Table-->	
	</td>
  </tr>
</table>
<!-- End Home Section-->
