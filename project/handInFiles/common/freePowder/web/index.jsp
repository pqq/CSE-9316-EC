<%@ page contentType="text/html" import="com.freePowder.beans.*,java.util.*"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>-   Freepowder.com   -</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/style.css" type="text/css">
<style type="text/css">
<!--
body {
	background-color: #77899B;
}
-->
</style></head>
<body background="images/bg.gif" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<% // Insert the shopping cart section %>
    <jsp:include page="cart.jsp" />
    
<% // Insert the header section %>
    <jsp:include page="header.jsp" />
	
<% // Check for the administrator %>
<% if(session.getAttribute("user") != null) { 
	UsrBean y = (UsrBean)session.getAttribute("user");
	String x = y.getLoginname(); 
    if(x.equalsIgnoreCase("Administrator")) {%>
	    <jsp:include page="admin-nav.jsp" />
<% } } %>

<% // Check which page we are on %>
<% if(request.getParameter("page") == null) { %>
    <jsp:include page="home.jsp" />
<% } else if(request.getParameter("page").equals("browse")){ %>
    <jsp:include page="browse.jsp" />
<% } else if(request.getParameter("page").equals("mydetails")){ %>
    <jsp:include page="mydetails.jsp" />
<% } else if(request.getParameter("page").equals("cart-details")){ %>
    <jsp:include page="cart-details.jsp" />
<% } else if(request.getParameter("page").equals("contact")){ %>
    <jsp:include page="contact.jsp" />
<% } else if(request.getParameter("page").equals("newuser")){ %>
    <jsp:include page="newuser.jsp" />
<% } else if(request.getParameter("page").equals("home")){ %>
    <jsp:include page="home.jsp" />
<% } else if(request.getParameter("page").equals("adminnews")){ %>
	<jsp:include page="adminnews.jsp" />
<% } else if(request.getParameter("page").equals("adminproducts")){ %>
<% } else if(request.getParameter("page").equals("adminsuppliers")){ %>
	<jsp:include page="adminsuppliers.jsp" />
<% } else if(request.getParameter("page").equals("adminusers")){ %>
<% } else if(request.getParameter("page").equals("adminweather")){ %>
<% } else { %>
    <jsp:include page="home.jsp" />
<% } %>
</body>
</html>
