/*
 * file:	BrowseBean.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	10. Nov 2003
 *
 */

package com.freePowder.beans;

import java.io.Serializable;


public class BrowseBean implements Serializable {

	private String brandname;
	private int product_pk;
	private String productname;
	private String cat_lev1;
	private String cat_lev2;
	private String cat_lev3;
	private String no_instock;
	private String no_sold;
	private String price;
	


	public BrowseBean() {
	}
	
	public int getProduct_pk() {
		return product_pk;
	}
	
	public void setProduct_pk(int product_pk) {
		this.product_pk = product_pk;
	}
	
	public String getBrandname() {
		return brandname;
	}
	
	public void setBransname(String brandname) {
		this.brandname = brandname;
	}
	
	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname){
		this.productname = productname;
	}

	public String getCat_lev1() {
		return cat_lev1;
	}

	public void setCat_lev1(String cat_lev1){
		this.cat_lev1 = cat_lev1;
	}

	public String getCat_lev2() {
		return cat_lev1;
	}

	public void setCat_lev2(String cat_lev1){
		this.cat_lev1 = cat_lev1;
	}

	public String getCat_lev3() {
		return cat_lev1;
	}

	public void setCat_lev3(String cat_lev1){
		this.cat_lev1 = cat_lev1;
	}

	public String getNo_instock() {
		return no_instock;
	}

	public void setNo_instock(String no_instock){
		this.no_instock = no_instock;
	}
	
	public String getNo_sold() {
		return no_sold;
	}

	public void setNo_sold(String no_sold){
		this.no_sold = no_sold;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price){
		this.price = price;
	}
	
}
