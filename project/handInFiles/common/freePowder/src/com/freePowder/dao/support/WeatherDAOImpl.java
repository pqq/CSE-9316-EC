/*
 * file:	WeatherDAOImpl.java
 * author:	Lukasz Klenner (lukasz@onepenguin.net)
 * started:	09. Nov 2003
 *
 */

package com.freePowder.dao.support;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import com.freePowder.beans.WeatherBean;
import com.freePowder.common.ServiceLocator;
import com.freePowder.common.ServiceLocatorException;
import com.freePowder.dao.DataAccessException;
import com.freePowder.dao.WeatherDAO;

import com.freePowder.utility.Debug;


public class WeatherDAOImpl implements WeatherDAO {
	
	private static ServiceLocator services;
	private static Debug d;
	
	
	/** Creates a new instance of WeatherDAOImpl */
	public WeatherDAOImpl() {
		d = new Debug("WeatherDAOImpl()");
		try {
			services = ServiceLocator.getInstance();
		} catch (ServiceLocatorException e) {
			e.printStackTrace();
		}
	}


	public List getWeather() throws DataAccessException {
		d.print("getWeather()");
		List list = new ArrayList();
		Connection con = null;
		try {
			d.print("getWeather().try...");
			con = services.getConnection();
			PreparedStatement stmt = con.prepareStatement("select location, report, temp, short, timestamp from weather");
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				d.print("getWeather().rs.next()");
				list.add(createWeatherBean(rs));
			}
		} catch (ServiceLocatorException e) {
			d.print("Unable to retrieve connection");
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			d.print("Unable to execute query; "+ e.getMessage());
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		return list;
	}



	private WeatherBean createWeatherBean(ResultSet rs) throws SQLException {
		WeatherBean bean = new WeatherBean();
		bean.setLocation(rs.getString("location"));
		bean.setReport(rs.getString("report"));
		bean.setTemp(rs.getInt("temp"));
		bean.setShort(rs.getString("short"));
		bean.setTimestamp(rs.getDate("timestamp"));
		return bean;
	}



}
