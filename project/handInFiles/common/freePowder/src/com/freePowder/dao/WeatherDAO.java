/*
 * file:	WeatherDAO.java
 * author:	Lukasz Klenner (lukasz@onepenguin.net)
 * started:	09. Nov 2003
 *
 */

package com.freePowder.dao;

import java.util.List;
import com.freePowder.beans.WeatherBean;


public interface WeatherDAO {

	List getWeather() throws DataAccessException;

}