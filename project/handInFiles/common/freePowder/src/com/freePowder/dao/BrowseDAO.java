/*
 * file:	BrowseDAO.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	10. Nov 2003
 *
 */

package com.freePowder.dao;

import java.util.List;
import com.freePowder.beans.BrowseBean;


public interface BrowseDAO {

	List getProducts() throws DataAccessException;

}
