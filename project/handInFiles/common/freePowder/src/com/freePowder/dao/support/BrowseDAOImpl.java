/*
 * file:	BrowseDAOImpl.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	10. Nov 2003
 *
 */

package com.freePowder.dao.support;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import com.freePowder.beans.BrowseBean;
import com.freePowder.common.ServiceLocator;
import com.freePowder.common.ServiceLocatorException;
import com.freePowder.dao.DataAccessException;
import com.freePowder.dao.BrowseDAO;

import com.freePowder.utility.Debug;


public class BrowseDAOImpl implements BrowseDAO {
	
	private static ServiceLocator services;
	private static Debug d;
	
	
	public BrowseDAOImpl() {
		d = new Debug("BrowseDAOImpl()");
		try {
			services = ServiceLocator.getInstance();
		} catch (ServiceLocatorException e) {
			e.printStackTrace();
		}
	}


	public List getProducts() throws DataAccessException {
		d.print("getProducts()");
		List list = new ArrayList();
		Connection con = null;
		try {
			d.print("getProducts().try...");
			con = services.getConnection();
			PreparedStatement stmt = con.prepareStatement("select news_pk, heading, body, timestamp from news");
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				d.print("getProducts().rs.next()");
				list.add(createBrowseBean(rs));
			}
		} catch (ServiceLocatorException e) {
			d.print("Unable to retrieve connection: "+e.getMessage());
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			d.print("Unable to execute query: "+e.getMessage());
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		return list;
	}



	private BrowseBean createBrowseBean(ResultSet rs) throws SQLException {
		BrowseBean bean = new BrowseBean();
/*		bean.setNews_pk(rs.getInt("news_pk"));
		bean.setHeading(rs.getString("heading"));
		bean.setBody(rs.getString("body"));
		bean.setTimestamp(rs.getDate("timestamp").toString());
*/		return bean;
	}



}
