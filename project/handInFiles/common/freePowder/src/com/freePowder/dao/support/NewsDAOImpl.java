/*
 * file:	NewsDAOImpl.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	07. Nov 2003
 *
 */

package com.freePowder.dao.support;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import com.freePowder.beans.NewsBean;
import com.freePowder.common.ServiceLocator;
import com.freePowder.common.ServiceLocatorException;
import com.freePowder.dao.DataAccessException;
import com.freePowder.dao.NewsDAO;

import com.freePowder.utility.Debug;


public class NewsDAOImpl implements NewsDAO {
	
	private static ServiceLocator services;
	private static Debug d;
	
	
	/** Creates a new instance of NewsDAOImpl */
	public NewsDAOImpl() {
		d = new Debug("NewsDAOImpl()");
		try {
			services = ServiceLocator.getInstance();
		} catch (ServiceLocatorException e) {
			e.printStackTrace();
		}
	}


	public List getNews() throws DataAccessException {
		d.print("getNews()");
		List list = new ArrayList();
		Connection con = null;
		try {
			d.print("getNews().try...");
			con = services.getConnection();
			PreparedStatement stmt = con.prepareStatement("select news_pk, heading, body, timestamp from news");
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				d.print("getNews().rs.next()");
				list.add(createNewsBean(rs));
			}
		} catch (ServiceLocatorException e) {
			d.print("Unable to retrieve connection");
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			d.print("Unable to execute query");
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		return list;
	}



	private NewsBean createNewsBean(ResultSet rs) throws SQLException {
		NewsBean bean = new NewsBean();
		bean.setNews_pk(rs.getInt("news_pk"));
		bean.setHeading(rs.getString("heading"));
		bean.setBody(rs.getString("body"));
		bean.setTimestamp(rs.getDate("timestamp").toString());
		return bean;
	}


    public void addNews(NewsBean bean) throws DataAccessException {
		d.print("addnews connection");
		Connection con = null;
		try {
			con = services.getConnection();
			PreparedStatement stmt = con.prepareStatement("insert into news (news_pk, heading, timestamp, body) values (news_seq.nextval, ?, to_date(?,'YYYY-MM-DD'), ?)");
			stmt.setString(1, bean.getHeading());
			stmt.setString(2, bean.getTimestamp());
			stmt.setString(3, bean.getBody());
			int n = stmt.executeUpdate();
			if (n != 1)
				throw new DataAccessException("Did not insert one row into database");
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			d.print(e.getMessage());
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
}
