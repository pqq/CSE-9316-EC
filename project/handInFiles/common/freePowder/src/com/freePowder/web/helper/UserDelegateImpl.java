/*
 * file:	UserDelegateImpl.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	02. Nov 2003
 *
 */

package com.freePowder.web.helper;

import com.freePowder.business.UserService;
import com.freePowder.business.support.UserServiceImpl;

import com.freePowder.utility.Debug;



public class UserDelegateImpl extends UserDelegate {

	private static UserDelegateImpl instance = new UserDelegateImpl();
	
	private UserService service;
	
	private UserDelegateImpl() {
		service = new UserServiceImpl();
		d = new Debug("RegisterCommand()");

	}
	
	public static UserDelegate getInstance() {
		return instance;
	}

	protected UserService getUserService() {
		return service;
	}

}
