#!/usr/local/bin/perl
#
# -----------------------------------------------------
# Filename:		getFile.pl
# Author:		Frode Klevstul (frode@klevstul.com)
# Started:		14.03.03
# Revision:		13.04.03
# -----------------------------------------------------


# -------------------
# use
# -------------------
use strict;
use LWP::Simple;

# -------------------
# global declarations
# -------------------
my $url = $ARGV[0];


# -------------------
# main
# -------------------
&downloadFile;

# -------------------
# sub procedures
# -------------------
sub downloadFile{

	my $result_file = $url;

	# sets the name of the result file
	if ($result_file =~ m/^.*\/(.*)$/){
		$result_file = $1;
	} else {
		$result_file = "getFile.result";
	}

	# checks if file already exists on OS
	if (-e $result_file){
		die "'$result_file' exists, please rename/delete existing file on OS to continue!\n";
	}
	print "Stores the file \n'$url' \ninto '$result_file'\n";
	
	# download the file
	my $file = &get($url);

	# write the file to disk
	open (FILE, ">$result_file") || die "couldn't open result file '$result_file'\n";
	print FILE $file;
	close (FILE);

	print "'$result_file' created\n";
}
