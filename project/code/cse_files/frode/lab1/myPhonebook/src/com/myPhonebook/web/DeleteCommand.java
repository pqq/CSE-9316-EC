/*
 * DeleteCommand.java
 *
 * Created on 9 August 2003, 11:21
 */

package com.myPhonebook.web;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This command is to remove a record from the database
 * @author  yunki
 */
public class DeleteCommand implements Command {
	
	/** Creates a new instance of DeleteCommand */
	public DeleteCommand() {
	}
	
	public String execute(HttpServletRequest request, HttpServletResponse response) 
		throws ServletException, IOException {

		// -- fk --
		PrintWriter out = response.getWriter();
		String CONTENT_TYPE = "text/html";		
		response.setContentType(CONTENT_TYPE);

		out.write("<HTML><BODY >");
        out.write("<H1>Operation DELETE has been invoked</H1>");
        out.write("</BODY></HTML>");
		// -- / --
		
		return null;
	}
	
}
