/*
 * LogoutCommand.java
 *
 * Created on 25 August 2003, 01:08
 */

package com.myPhonebook.web;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This is the command that will be used for logging in users.
 * If logon is successful, the command should place a list of phonebook entries
 * in the request attriubutes.
 * @author  yunki
 */
public class LogoutCommand implements Command {
	
	/** Creates a new instance of LogoutCommand */
	public LogoutCommand() {
	}
	
	public String execute(HttpServletRequest request, HttpServletResponse response) 
	throws ServletException, IOException {
	
		//TODO: set the response content type to "text/html" here
		//TODO: You'll have to use getWriter() to output some HTML texts 
		//TODO: close the output stream
		
		// -- fk --
		PrintWriter out = response.getWriter();
		String CONTENT_TYPE = "text/html";		
		response.setContentType(CONTENT_TYPE);

		out.write("<HTML><BODY >");
        out.write("<H1>Operation LOGOUT has been invoked</H1>");
        out.write("</BODY></HTML>");
		// -- / --
		
		return null;
	}
	
}
