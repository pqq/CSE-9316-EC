/*
 * PhonebookDAOImpl.java
 *
 * Created on 9 August 2003, 14:42
 */

package com.myPhonebook.dao.support;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.myPhonebook.beans.PhonebookBean;
import com.myPhonebook.common.ServiceLocator;
import com.myPhonebook.common.ServiceLocatorException;
import com.myPhonebook.dao.DataAccessException;
import com.myPhonebook.dao.PhonebookDAO;
/**
 * The Data Access Object for phone book records.
 * This object performs the database operations required for maintaining phone
 * book records.
 * @author  $autho
 */
public class PhonebookDAOImpl implements PhonebookDAO {

	/**
	 * The service locator to retrieve database connections from
	 */
	private ServiceLocator services;


	private boolean debug = false;

	private void debug(String str){
		if (debug)
			System.out.println("DEBUG: "+str);
	}


	/** Creates a new instance of PhonebookDAOImpl */
	public PhonebookDAOImpl() {
		try {
			services = ServiceLocator.getInstance();
		} catch (ServiceLocatorException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see com.myPhonebook.dao.PhonebookDAO#insert(com.myPhonebook.beans.PhonebookBean)
	 */
	public void insert(PhonebookBean phonebookBean) throws DataAccessException {
		debug("insert(phonebookBean)");

		Connection con = null;
		try {
			con = services.getConnection();
			PreparedStatement stmt = con.prepareStatement("insert into phonebook_record values (phonebook_record_id.nextval, ?, ?, ?, ?, ?)");
			stmt.setInt(1, phonebookBean.getOwner());
			stmt.setString(2, phonebookBean.getShortName());
			stmt.setString(3, phonebookBean.getFullName());
			stmt.setString(4, phonebookBean.getEmail());
			stmt.setString(5, phonebookBean.getContactNumber());
			int n = stmt.executeUpdate();
			if (n != 1)
				throw new DataAccessException("Did not insert one row into database");
			if (stmt != null)
				stmt.close();
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	/**
	 * @see com.myPhonebook.dao.PhonebookDAO#delete(int)
	 */
	public void delete(int id) throws DataAccessException {
		debug("delete("+id+")");

		Connection con = null;
		try {
			con = services.getConnection();
			Statement stmt = con.createStatement();
			int n = stmt.executeUpdate("delete from phonebook_record where id="+id);
			if (n != 1)
				throw new DataAccessException("Record with id="+id+" is not deleted from phonebook_record");
			if (stmt != null)
				stmt.close();
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	/**
	 * @see com.myPhonebook.dao.PhonebookDAO#findAllForUser(int)
	 */
	public List findAllByUser(int id) throws DataAccessException {
		debug("findAllByUser("+id+")");
		List list = new ArrayList();

		Connection con = null;

		try {
			con = services.getConnection();
			ResultSet resSet;
			PreparedStatement stmt = con.prepareStatement("select id from phonebook_record where owner=?");
			stmt.setInt(1,id);

			resSet = stmt.executeQuery();
			int n = resSet.getFetchSize();
			if (n < 1)
				throw new DataAccessException("No entry found");

			while (resSet.next()) {
				list.add(getPhonebook(resSet.getInt("id")));
			}

			if (stmt != null)
				stmt.close();
			if (resSet != null)
				resSet.close();

		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}


		return list;
	}

	/**
	 * @see com.myPhonebook.dao.PhonebookDAO#getPhonebook(int)
	 */
	public PhonebookBean getPhonebook(int id) throws DataAccessException {
		debug("getPhonebook("+id+")");

		Connection con = null;
		PhonebookBean p = new PhonebookBean();

		try {
			con = services.getConnection();
			ResultSet resSet;

			PreparedStatement stmt = con.prepareStatement("select * from phonebook_record where id=?");
			stmt.setInt(1,id);
			resSet = stmt.executeQuery();

			int num = 0;

			while (resSet.next()){
				num++;		// a counter that keeps track of the row_count
				p.setId(resSet.getInt("id"));
				p.setOwner(resSet.getInt("owner"));
				p.setShortName(resSet.getString("short_name"));
				p.setFullName(resSet.getString("full_name"));
				p.setEmail(resSet.getString("email"));
				p.setContactNumber(resSet.getString("contact_number"));
			}

			if (num==0)	// if we don't have any entries we return null
				return null;
			if (resSet != null)
				stmt.close();
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		return p;
	}
}
