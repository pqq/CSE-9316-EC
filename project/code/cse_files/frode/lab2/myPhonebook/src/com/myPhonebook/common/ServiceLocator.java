/*
 * ServiceLocator.java
 * Created on 10/08/2003
 *
 */
package com.myPhonebook.common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import javax.naming.Context;		// fk
import javax.naming.InitialContext;	// fk
import javax.naming.NamingException;	// fk

/**
 * @author $author
 */
public class ServiceLocator {

	private static ServiceLocator instance;

	private Properties env;

	private DataSource ds;

	private Connection con;

	public static ServiceLocator getInstance() throws ServiceLocatorException {
		if (instance == null)
			instance = new ServiceLocator();
		return instance;
	}

	private ServiceLocator() throws ServiceLocatorException {
		/*try {
			Class.forName("oracle.jdbc.driver.OracleDriver");

		} catch (ClassNotFoundException e) {
			throw new ServiceLocatorException("Unable to find class for database connection; " + e.getMessage(), e);
		}
		*/

		// --fk--
		env = new Properties();
                env.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
                env.put(Context.PROVIDER_URL, "t3://localhost:8890");
		env.put(Context.SECURITY_PRINCIPAL, "ecom12");
		env.put(Context.SECURITY_CREDENTIALS, "kickass12");
                try {
                        ds = getDataSource();
  		} catch (ServiceLocatorException e) {
                        e.printStackTrace();
                }
		// --/--

	}

	/**
	 * Finds a data source by looking up the initial context
	 * @return
	 * @throws ServiceLocatorException
	 */
	public DataSource getDataSource() throws ServiceLocatorException {
		//return ds;
		if (ds == null) {
  		try {
                	InitialContext ctx = new InitialContext(env);
                        ds = (DataSource)ctx.lookup("cs9316DataSource");
		} catch (NamingException e) {
			throw new ServiceLocatorException("Unable to locate data source; " + e.getMessage(), e);
		}
		}
		return ds;
	}

	/**
	 * Gets a connection to the database.
	 * @return
	 * @throws ServiceLocatorException
	 */
	public Connection getConnection() throws ServiceLocatorException {
		/*try {
			con = DriverManager.getConnection("jdbc:oracle:thin:@thylacine.cse.unsw.edu.au:1521:cse8", "ecom12", "kickass12");
			return con;
		} catch (SQLException e) {
			throw new ServiceLocatorException("Unable to open connection to data source; " + e.getMessage(), e);
		}
		*/
		try {
			return getDataSource().getConnection();
		} catch (SQLException e) {
			throw new ServiceLocatorException("Unable to open connection to data source; " + e.getMessage(), e);
		}
	}
}
