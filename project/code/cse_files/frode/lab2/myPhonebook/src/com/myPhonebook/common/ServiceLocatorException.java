/*
 * ServiceLocatorException.java
 * Created on 10/08/2003
 *
 */
package com.myPhonebook.common;

/**
 * Exception that is thrown when a service cannot be located or loaded
 * @author yunki
 */
public class ServiceLocatorException extends NestedException {


	/**
	 * Constructs an exception with the given message
	 * @param message
	 */
	public ServiceLocatorException(String message) {
		super(message);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ServiceLocatorException(String message, Throwable cause) {
		super(message, cause);
	}

}
