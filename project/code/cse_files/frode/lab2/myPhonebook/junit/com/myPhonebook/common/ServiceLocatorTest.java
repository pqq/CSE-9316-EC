/*
 * ServiceLocatorTest.java
 * Created on 23/08/2003
 * 
 * Copyright 2003 DiMETRO Software
 */
package com.myPhonebook.common;

import junit.framework.TestCase;

/**
 * 
 * @author yunki
 */
public class ServiceLocatorTest extends TestCase {

	/**
	 * Constructor for ServiceLocatorTest.
	 * @param arg0
	 */
	public ServiceLocatorTest(String arg0) {
		super(arg0);
	}

	public static void main(String[] args) {
		junit.swingui.TestRunner.run(ServiceLocatorTest.class);
	}

	public void testCreateConnection() throws Exception {
		ServiceLocator services = ServiceLocator.getInstance();
		assertNotNull(services.getConnection());
	}
}
