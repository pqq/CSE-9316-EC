drop table users;
create table users (
   id int primary key,
   firstname varchar2(20),
   lastname varchar2(20),
   access_level int,
   username varchar2(20) not null,
   password varchar2(20) not null
);

drop table phonebook_record;
create table phonebook_record (
   id int primary key,
   owner int,
   short_name varchar2(20),
   full_name varchar2(200),
   email varchar2(60), 
   contact_number varchar2(15) 
);
