clear
echo "*** Compiling and deploying freePowder ***"

# ------------------------
# changing to right directory
# ------------------------
cd /home/ecom12/private/frode/fpProject/java/freePowder

# ------------------------
# setting classpaths
# ------------------------
export CLASSPATH='.'
source /home/ecom12/bea/user_projects/freePowder/setEnv.sh

echo "*** Classpath is set ***"
#echo CLASSPATH=${CLASSPATH}

# ------------------------
# copying files into build directory
# ------------------------
cp web/*.jsp build/
cp web/WEB-INF/web.xml build/WEB-INF/

# ------------------------
# compiling .java
# ------------------------
cd src
javac -d ../build/WEB-INF/classes com/freePowder/web/*.java
javac -d ../build/WEB-INF/classes com/freePowder/common/*.java
javac -d ../build/WEB-INF/classes com/freePowder/beans/*.java
#javac -d ../build/WEB-INF/classes com/myPhonebook/dao/support/*.java
#javac -d ../build/WEB-INF/classes com/myPhonebook/dao/*.java

echo "*** Javafiles are compiled ***"
 
# ---------------------------
# deplyoing the application
# ---------------------------
cd ../build
jar -cf freePowder.war *
cp freePowder.war /home/ecom12/bea/user_projects/freePowder/applications/

echo "*** The application is deployed ***"
