/*
 * file:	DelegateFactory.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	02. Nov 2003
 *
 */

package com.freePowder.web.helper;

import java.util.HashMap;



public class DelegateFactory {

	private HashMap delegates = new HashMap();

	private static DelegateFactory instance = new DelegateFactory();

	public static DelegateFactory getInstance() {
		return instance;
	}
		
	private DelegateFactory() {
		delegates.put("User", UserDelegateImpl.getInstance());		
	}

	public UserDelegate getUserDelegate() {
		return (UserDelegate) delegates.get("User");
	}
}
