/*
 * file:	DataAccessException.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	02. Nov 2003
 *
 */

package com.freePowder.dao;

import com.freePowder.common.NestedException;



public class DataAccessException extends NestedException {


	public DataAccessException(String message) {
		super(message);
	}

	public DataAccessException(String message, Throwable cause) {
		super(message, cause);
	}

}
