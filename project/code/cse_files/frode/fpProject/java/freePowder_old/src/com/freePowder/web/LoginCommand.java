/*
 * file:	LoginCommand.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	25. Oct 2003
 *
 */

package com.freePowder.web;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.freePowder.beans.UsrBean;
import com.freePowder.business.UserLoginFailedException;
import com.freePowder.web.helper.DelegateFactory;
import com.freePowder.web.helper.UserDelegate;





public class LoginCommand implements Command {

	private static UserDelegate userDelegate;	
	public LoginCommand() {
		userDelegate = userDelegate = DelegateFactory.getInstance().getUserDelegate();
	}
	
	public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		try {
			System.out.println("DEBUG: LoginCommand.execute()");

			UsrBean user = userDelegate.login(request.getParameter("loginname"), request.getParameter("password"));
			if (user == null) {
				return "/loginfailed.jsp";
			}
			HttpSession session = request.getSession();
			session.setAttribute("user", user);

			return "/loginsuccess.jsp";
		} catch (UserLoginFailedException e) {
			System.out.println("DEBUG: UserLoginFailedException"); // DEBUG *** DEBUG *** DEBUG

			//e.printStackTrace();
			return "/loginfailed.jsp";
		}
	}
	
	
}
