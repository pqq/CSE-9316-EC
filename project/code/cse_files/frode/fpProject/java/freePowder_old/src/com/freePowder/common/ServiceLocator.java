/*
 * file:	ServiceLocator.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	25. Oct 2003
 *
 */

package com.freePowder.common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;


public class ServiceLocator {

	private static ServiceLocator instance;

	private Properties env;

	private DataSource ds;

	private Connection con;

	public static ServiceLocator getInstance() throws ServiceLocatorException {
		if (instance == null)
			instance = new ServiceLocator();
		return instance;
	}

	private ServiceLocator() throws ServiceLocatorException {
		env = new Properties();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
		env.put(Context.PROVIDER_URL, "t3://localhost:9018");
		env.put(Context.SECURITY_PRINCIPAL, "fp");
		env.put(Context.SECURITY_CREDENTIALS, "freePowder8");
		try {
			ds = getDataSource();
  		} catch (ServiceLocatorException e) {
			e.printStackTrace();
		}
	}


	public DataSource getDataSource() throws ServiceLocatorException {
		if (ds == null) {
	  		try {
				InitialContext ctx = new InitialContext(env);
				ds = (DataSource)ctx.lookup("test");
			} catch (NamingException e) {
				throw new ServiceLocatorException("Unable to locate data source; " + e.getMessage(), e);
			}
		}
		return ds;
	}



	public Connection getConnection() throws ServiceLocatorException {
		/*try {
			con = DriverManager.getConnection("jdbc:oracle:thin:@thylacine.cse.unsw.edu.au:1521:cse8", "ecom12", "kickass12");
			return con;
		} catch (SQLException e) {
			throw new ServiceLocatorException("Unable to open connection to data source; " + e.getMessage(), e);
		}
		*/
		try {
			return getDataSource().getConnection();
		} catch (SQLException e) {
			throw new ServiceLocatorException("Unable to open connection to data source; " + e.getMessage(), e);
		}
	}
}
