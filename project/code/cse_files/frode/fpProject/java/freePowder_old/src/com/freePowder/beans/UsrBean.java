/*
 * file:	UserBean.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	27. Oct 2003
 *
 */

package com.freePowder.beans;

import java.io.Serializable;

/*
 * file:	UserDAOImpl.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	30. Oct 2003
 *
 */

public class UsrBean implements Serializable {

	private int usr_pk;
	private String loginname;
	private String password;
	private String first_name;
	private String last_name;
	private String email;
	private String sex;
	private String address;
	private String zip;
	private int location_fk;
	private String last_login;


	/** Creates a new instance of Usr */
	public UsrBean() {
	}
	
	public int getUsr_pk() {
		return usr_pk;
	}
	
	public void setUsr_pk(int id) {
		this.usr_pk = usr_pk;
	}
	
	public String getLoginname() {
		return loginname;
	}
	
	public void setLoginname(String loginname) {
		this.loginname = loginname;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name){
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name){
		this.last_name = last_name;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex){
		this.sex = sex;
	}
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip){
		this.zip = zip;
	}

	public int getLocation_fk() {
		return location_fk;
	}
	
	public void setLocation_fk(int location_fk) {
		this.location_fk = location_fk;
	}

	public String getLast_login() {
		return last_login;
	}

	public void setLast_login(String last_login){
		this.last_login = last_login;
	}
	
	public String toString() {
		return "[USR]" + loginname + " " + password + " " + first_name+ " " + last_name;
	}
}
