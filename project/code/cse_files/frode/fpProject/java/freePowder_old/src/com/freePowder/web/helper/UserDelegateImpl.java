/*
 * file:	UserDelegateImpl.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	02. Nov 2003
 *
 */

package com.freePowder.web.helper;

import com.freePowder.business.UserService;
import com.freePowder.business.support.UserServiceImpl;



public class UserDelegateImpl extends UserDelegate {

	private static UserDelegateImpl instance = new UserDelegateImpl();
	
	private UserService service;
	
	private UserDelegateImpl() {
		service = new UserServiceImpl();
	}
	
	public static UserDelegate getInstance() {
		return instance;
	}

	protected UserService getUserService() {
		System.out.println("DEBUG: UserDelegateImpl.getUserService()");
		return service;
	}

}
