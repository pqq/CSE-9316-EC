/*
 * file:	DAOFactory.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	02. Nov 2003
 *
 */

package com.freePowder.dao;

import com.freePowder.dao.support.UserDAOImpl;
import java.util.HashMap;
import java.util.Map;



public class DAOFactory {
	
	private static final String USER_DAO = "userDAO";
	
	private Map daos;
	
	private static DAOFactory instance = new DAOFactory();
	
	/** Creates a new instance of DAOFactory */
	private DAOFactory() {
		daos = new HashMap();
		daos.put(USER_DAO, new UserDAOImpl());
	}
	
	public UserDAO getUserDAO() {
		return (UserDAO) daos.get(USER_DAO);
	}

	public static DAOFactory getInstance() {
		return instance;
	}
}
