/*
 * file:	ControllerServlet.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	25. Oct 2003
 *
 */

package com.freePowder.web;

import java.io.*;
import java.net.*;

import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;




public class ControllerServlet extends HttpServlet {
	
	private Map commands;
	
	/** Initializes the servlet.
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		commands = new HashMap();
		commands.put("login", new LoginCommand());
		commands.put("logout", new LogoutCommand());
		commands.put("getpassword", new GetPasswordCommand());
		commands.put("register", new RegisterCommand());
		commands.put("getdetails", new GetDetailsCommand());
		commands.put("search", new SearchCommand());
		commands.put("browse", new BrowseCommand());
		commands.put("showproduct", new ShowProductCommand());
		commands.put("showproductdetails", new ShowProductDetailsCommand());
		commands.put("additem", new AddItemCommand());
		commands.put("removeitem", new RemoveItemCommand());
		commands.put("listitems", new ListItemsCommand());
		commands.put("checkout", new CheckoutCommand());
		commands.put("changenoitem", new ChangeNoItemCommand());
		commands.put("addshippingdetails", new AddShippingDetailsCommand());
		commands.put("addpaymentdetails", new AddPaymentDetailsCommand());
		commands.put("submitorder", new SubmitOrderCommand());
		commands.put("listusers", new ListUsersCommand());
		commands.put("deleteuser", new DeleteUserCommand());
		commands.put("listsuppliers", new ListSuppliersCommand());
		commands.put("updatesupplier", new UpdateSupplierCommand());
		commands.put("updatesupplierproduct", new UpdateSupplierProductCommand());
		commands.put("addnews", new AddNewsCommand());
		commands.put("removenews", new RemoveNewsCommand());
		commands.put("bulkemail", new BulkEmailCommand());
		commands.put("viewsalesreport", new ViewSalesReportCommand());
		commands.put("PAGE_NOT_FOUND", new ErrorCommand());
	}
	
	/** Destroys the servlet.
	 */
	public void destroy() {
		
	}
	
asdasd

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {

		System.out.println("DEBUG: ControllerServlet.processRequest");

		Command cmd = resolveCommand(request);

		System.out.println("DEBUG: ControllerServlet.processRequest 2");

		String next = cmd.execute(request, response);

		System.out.println("DEBUG: ControllerServlet.processRequest 3");

		if (next == null)
			next = "";
		if (next.indexOf('.') < 0) {
			cmd = (Command) commands.get(next);
			next = cmd.execute(request, response);
		}		

		System.out.println("DEBUG: ControllerServlet.processRequest 4");

		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(next);

		System.out.println("DEBUG: ControllerServlet.processRequest 5");

		dispatcher.forward(request, response);

		System.out.println("DEBUG: ControllerServlet.processRequest 6");
	}
	
	private Command resolveCommand(HttpServletRequest request) {
		Command cmd = (Command) commands.get(request.getParameter("operation"));
		if (cmd == null) {
			cmd = (Command) commands.get("PAGE_NOT_FOUND");
		}
		return cmd;
	}

	
	/** Handles the HTTP <code>GET</code> method.
	 * @param request servlet request
	 * @param response servlet response
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		processRequest(request, response);
	}
	
	/** Handles the HTTP <code>POST</code> method.
	 * @param request servlet request
	 * @param response servlet response
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		processRequest(request, response);
	}
	
	/** Returns a short description of the servlet.
	 */
	public String getServletInfo() {
		return "Short description";
	}
	
}
