/*
 * DeleteCommand.java
 *
 * Created on 9 August 2003, 11:21
 */

package com.myPhonebook.web;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This command is to remove a record from the database
 * @author  yunki
 */
public class DeleteCommand implements Command {
	
	/** Creates a new instance of DeleteCommand */
	public DeleteCommand() {
	}
	
	public String execute(HttpServletRequest request, HttpServletResponse response) 
		throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("The operation delete was requested");
		out.close();		
		
		return null;
	}
	
}
