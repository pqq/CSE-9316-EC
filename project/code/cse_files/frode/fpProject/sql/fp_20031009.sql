/*==============================================================*/
/* DBMS name:      ORACLE Version 8i2 (8.1.6)                   */
/* Created on:     9/10/2003 3:49:59 PM                         */
/*==============================================================*/


alter table PRODUCT
   drop constraint FK_PRODUCT_REFERENCE_BRAND;

alter table PRODUCT
   drop constraint FK_PRODUCT_REFERENCE_SUPPLIER;

alter table PRODUCT_SPECIFICATION
   drop constraint FK_PRODUCT__REFERENCE_PRODUCT;

alter table PRODUCT_SPECIFICATION
   drop constraint FK_PRODUCT__REFERENCE_SPECIFIC;

alter table SUPPLIER_PRODUCT
   drop constraint FK_SUPPLIER_REFERENCE_PRODUCT;

alter table SUPPLIER_PRODUCT
   drop constraint FK_SUPPLIER_REFERENCE_SUPPLIER;

alter table USR
   drop constraint FK_USR_REFERENCE_LOCATION;

drop table BRAND cascade constraints;

drop table LOCATION cascade constraints;

drop table NEWS cascade constraints;

drop table PRODUCT cascade constraints;

drop table PRODUCT_SPECIFICATION cascade constraints;

drop table SALES_LOG cascade constraints;

drop table SPECIFICATION cascade constraints;

drop table SUPPLIER cascade constraints;

drop table SUPPLIER_PRODUCT cascade constraints;

drop table USR cascade constraints;

drop table WEATHER cascade constraints;

/*==============================================================*/
/* Table: BRAND                                                 */
/*==============================================================*/
create table BRAND  (
   BRAND_PK             INT                             not null,
   BRANDNAME            VARCHAR2(64),
   constraint PK_BRAND primary key (BRAND_PK)
);

comment on table BRAND is
'The product''s brand';

/*==============================================================*/
/* Table: LOCATION                                              */
/*==============================================================*/
create table LOCATION  (
   LOCATION_PK          INT                             not null,
   LOCATIONNAME         VARCHAR2(64),
   constraint PK_LOCATION primary key (LOCATION_PK)
);

comment on table LOCATION is
'Specifies the state the user lives in';

/*==============================================================*/
/* Table: NEWS                                                  */
/*==============================================================*/
create table NEWS  (
   NEWS_PK              INT                             not null,
   HEADING              VARCHAR2(256),
   BODY                 VARCHAR2(4000),
   TIMESTAMP            DATE,
   constraint PK_NEWS primary key (NEWS_PK)
);

comment on table NEWS is
'Portal-news from FreePowder.com''s crew';

/*==============================================================*/
/* Table: PRODUCT                                               */
/*==============================================================*/
create table PRODUCT  (
   PRODUCT_PK           INT                             not null,
   PRODUCTNAME          VARCHAR2(64),
   BRAND_FK             INT,
   SUPPLIER_FK          INT,
   CAT_LEV1             VARCHAR2(64),
   CAT_LEV2             VARCHAR2(64),
   CAT_LEV3             VARCHAR2(64),
   PICTURE              VARCHAR2(32),
   constraint PK_PRODUCT primary key (PRODUCT_PK)
);

comment on table PRODUCT is
'General information about a product';

/*==============================================================*/
/* Table: PRODUCT_SPECIFICATION                                 */
/*==============================================================*/
create table PRODUCT_SPECIFICATION  (
   PRODUCT_FK           INT                             not null,
   SPECIFICATION_FK     INT                             not null,
   VALUE                VARCHAR2(64),
   constraint PK_PRODUCT_SPECIFICATION primary key (PRODUCT_FK, SPECIFICATION_FK)
);

comment on table PRODUCT_SPECIFICATION is
'Specification details about a product, like size, color, material etc.';

/*==============================================================*/
/* Table: SALES_LOG                                             */
/*==============================================================*/
create table SALES_LOG  (
   SALES_LOG_PK         INT                             not null,
   PRODUCT_PK           INT,
   BRANDNAME            VARCHAR2(64),
   PRODUCTNAME          VARCHAR2(64),
   USR_PK               INT,
   IN_PRICE             INT,
   OUT_PRICE            INT,
   NO_ITEMS             INT,
   ORDER_DATE           DATE,
   SHIPPING_DATE        DATE,
   SHIPPING_ADDRESS     VARCHAR2(2000),
   SHIPPING_STATUS      VARCHAR2(32),
   constraint PK_SALES_LOG primary key (SALES_LOG_PK)
);

comment on table SALES_LOG is
'Keep tracks of sales and shipping information';

/*==============================================================*/
/* Table: SPECIFICATION                                         */
/*==============================================================*/
create table SPECIFICATION  (
   SPECIFICATION_PK     INT                             not null,
   NAME                 VARCHAR2(64),
   SUFFIX               VARCHAR2(3),
   constraint PK_SPECIFICATION primary key (SPECIFICATION_PK)
);

comment on table SPECIFICATION is
'General specifications';

/*==============================================================*/
/* Table: SUPPLIER                                              */
/*==============================================================*/
create table SUPPLIER  (
   SUPPLIER_PK          INT                             not null,
   SUPPLIERNAME         VARCHAR2(32),
   XML_URL              VARCHAR2(128),
   UPDATE_URL           VARCHAR2(128),
   LAST_UPDATE          DATE,
   constraint PK_SUPPLIER primary key (SUPPLIER_PK)
);

comment on table SUPPLIER is
'The supplier/partner that delivers XML-feed to FreePowder.com';

/*==============================================================*/
/* Table: SUPPLIER_PRODUCT                                      */
/*==============================================================*/
create table SUPPLIER_PRODUCT  (
   SUPPLIER_FK          INT                             not null,
   PRODUCT_FK           INT                             not null,
   NO_INSTOCK           INT,
   NO_SOLD              INT,
   MARKUP               INT,
   ACTIVATED            INT,
   ONSPECIAL            INT,
   PRICE                INT,
   constraint PK_SUPPLIER_PRODUCT primary key (SUPPLIER_FK, PRODUCT_FK)
);

comment on table SUPPLIER_PRODUCT is
'Supplier spesific information about a product';

/*==============================================================*/
/* Table: USR                                                   */
/*==============================================================*/
create table USR  (
   USR_PK               INT                             not null,
   LOGINNAME            VARCHAR2(32),
   PASSWORD             VARCHAR2(32),
   FIRST_NAME           VARCHAR2(64),
   LAST_NAME            VARCHAR2(64),
   EMAIL                VARCHAR2(128),
   SEX                  VARCHAR2(1),
   ADDRESS              VARCHAR2(1000),
   ZIP                  VARCHAR2(8),
   LOCATION_FK          INT,
   LAST_LOGIN           DATE,
   constraint PK_USR primary key (USR_PK)
);

comment on table USR is
'Contains all user information';

/*==============================================================*/
/* Table: WEATHER                                               */
/*==============================================================*/
create table WEATHER  (
   LOCATION             VARCHAR2(32)                    not null,
   REPORT               VARCHAR2(4000),
   TIMESTAMP            DATE,
   constraint PK_WEATHER primary key (LOCATION)
);

comment on table WEATHER is
'Weather report';

alter table PRODUCT
   add constraint FK_PRODUCT_REFERENCE_BRAND foreign key (BRAND_FK)
      references BRAND (BRAND_PK);

alter table PRODUCT
   add constraint FK_PRODUCT_REFERENCE_SUPPLIER foreign key (SUPPLIER_FK)
      references SUPPLIER (SUPPLIER_PK);

alter table PRODUCT_SPECIFICATION
   add constraint FK_PRODUCT__REFERENCE_PRODUCT foreign key (PRODUCT_FK)
      references PRODUCT (PRODUCT_PK);

alter table PRODUCT_SPECIFICATION
   add constraint FK_PRODUCT__REFERENCE_SPECIFIC foreign key (SPECIFICATION_FK)
      references SPECIFICATION (SPECIFICATION_PK);

alter table SUPPLIER_PRODUCT
   add constraint FK_SUPPLIER_REFERENCE_PRODUCT foreign key (PRODUCT_FK)
      references PRODUCT (PRODUCT_PK);

alter table SUPPLIER_PRODUCT
   add constraint FK_SUPPLIER_REFERENCE_SUPPLIER foreign key (SUPPLIER_FK)
      references SUPPLIER (SUPPLIER_PK);

alter table USR
   add constraint FK_USR_REFERENCE_LOCATION foreign key (LOCATION_FK)
      references LOCATION (LOCATION_PK);

