delete from usr where usr_pk < 0;

insert into usr
(usr_pk, loginname, password, first_name, last_name)
values(-1, 'fp', 'pwd', 'freepowder', '.com');

insert into usr
(usr_pk, loginname, password, first_name, last_name)
values(-2, 'f', 'k', 'Frode', 'Klevstul');

insert into usr
(usr_pk, loginname, password, first_name, last_name)
values(-3, 'a', 'b', 'Abraham', 'Bollocks');

insert into usr
(usr_pk, loginname, password, first_name, last_name)
values(-4, 'l', 'k', 'Lukasz', 'K');

commit;
