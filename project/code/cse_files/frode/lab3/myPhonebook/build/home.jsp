<%@ page import="java.util.*,com.myPhonebook.beans.*" %>
<%@ page contentType="text/html" %>
<% /*

	 dont forget to obtain instances of the beans you need:
	 1. entry - which is a PhonebookBean
	 2. list  - ArrayList, comes from request
	 3. user  - UserBean and comes from the session
*/%>


<jsp:useBean id="entry" class="com.myPhonebook.beans.PhonebookBean" scope="page"/>
<jsp:useBean id="list" class="java.util.ArrayList" scope="request"/>
<jsp:useBean id="user" class="com.myPhonebook.beans.UserBean" scope="session"/>

<html>

<h2>Welcome <i><%=user.getFirstname() %> <%=user.getLastname() %></i></h2>


<table width="100%" border=1>
<tr>
<th>Short Name</th><TH>Complete Name</TH><TH>Email</TH><TH>Phone</TH><th>&nbsp;</th></tr>
<% for (int i = 0; i < list.size(); i++) { 
	entry = (PhonebookBean) list.get(i);
%>
	<tr>
		<td><%=entry.getShortName() %></td>
		<Td><%=entry.getFullName() %></Td>
		<Td><%=entry.getEmail() %></Td>
		<Td><%=entry.getContactNumber() %></Td>
		<td><a href="dispatcher?operation=delete&id=<%=entry.getId() %>">Delete</a></TD>
	</tr>
<% } %>
</TABLE>
<hr>
To add a new entry please complete the following information:
<p>
<%/*
put the form that is used to add users here
The action should be dispatcher, method post
There should be 4 text input boxes:
	1. shortName of type text
	2. completeName of type text
	3. email of type text
	4. phone of type text

To specify the operation "add", you will need to use a hidden input
and finally dont forget the submit button
*/%>

<form action='dispatcher' method="post">
<input type="hidden" name="operation" value="add">
<table border="1">
<tr>
	<td>Short Name</td>
	<td><input type="text" name="shortName"></td>
</tr>
<tr>
	<td>Complete Name</td>
	<td><input type="text" name="completeName"></td>
</tr>
<tr>
	<td>Email</td>
	<td><input type="text" name="email"></td>
</tr>
<tr>
	<td>Phone</td>
	<td><input type="text" name="phone"></td>
</tr>
<tr><td colspan="2" align="left"><input type="submit" value="Add"></td></tr>

</form>

<p>
<a href="dispatcher?operation=logout">Logout</a>
</html>
