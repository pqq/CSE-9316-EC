<%@ page contentType="text/html" import="com.myPhonebook.beans.PhonebookBean"%>
<jsp:useBean beanName="entry" type="com.myPhonebook.beans.PhonebookBean" scope="request" id="entry"/>

<html>
The entry <jsp:getProperty name="entry" property="shortName"/> has been deleted.
<p>
<a href="dispatcher?operation=list">Return to home</a>
</html>
