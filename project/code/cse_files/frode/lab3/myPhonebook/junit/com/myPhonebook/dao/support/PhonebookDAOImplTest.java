/*
 * PhonebookDAOImplTest.java
 * Created on 10/08/2003
 *
 */
package com.myPhonebook.dao.support;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import com.myPhonebook.beans.PhonebookBean;
import com.myPhonebook.common.ServiceLocator;

import junit.framework.TestCase;

/**
 * @author yunki
 */
public class PhonebookDAOImplTest extends TestCase {

	/**
	 * Constructor for PhonebookDAOImplTest.
	 * @param arg0
	 */
	public PhonebookDAOImplTest(String arg0) {
		super(arg0);
	}

	public static void main(String[] args) {
		junit.swingui.TestRunner.run(PhonebookDAOImplTest.class);
	}

	public void testFindAllByUser() throws Exception {
		PhonebookDAOImpl phonebookDao = new PhonebookDAOImpl();
		List list = phonebookDao.findAllByUser(1);
		assertEquals(2, list.size());
	}
	
	public void testAddPhonebookEntry() throws Exception {
		PhonebookBean p = new PhonebookBean();
		p.setContactNumber("123");
		p.setEmail("yunki@cse.unsw.edu.au");
		p.setFullName("full name");
		p.setOwner(2);
		p.setShortName("shortName");
		PhonebookDAOImpl phonebookDao = new PhonebookDAOImpl();
		phonebookDao.insert(p);
	}
	
	public void testDeletePhonebookEntry() throws Exception {
		PhonebookDAOImpl phonebookDao = new PhonebookDAOImpl();
		PhonebookBean p = phonebookDao.getPhonebook(3);
		assertNotNull(p);
		phonebookDao.delete(3);
		PhonebookBean q = phonebookDao.getPhonebook(3);
		assertNull(q);
		// have to restore the database back into its original state
		try {
			ServiceLocator services = ServiceLocator.getInstance();
			Connection con = services.getConnection();
			PreparedStatement stmt = con.prepareStatement("insert into phonebook_record values (?,?,?,?,?,?)");
			stmt.setInt(1, p.getId());
			stmt.setInt(2, p.getOwner());
			stmt.setString(3, p.getShortName());
			stmt.setString(4, p.getFullName());
			stmt.setString(5, p.getEmail());
			stmt.setString(6, p.getContactNumber());
			stmt.executeUpdate();
		} catch (SQLException e) {
			fail("Unable to restore database state; " + e.getMessage());
		}
	}
	
	public void testGetPhonebook() throws Exception {
		PhonebookDAOImpl phonebookDao = new PhonebookDAOImpl();
		PhonebookBean p = phonebookDao.getPhonebook(1);
		assertNotNull(p);
		assertEquals("Boualem", p.getShortName());
		assertEquals("Boualem Benetallah", p.getFullName());
		assertEquals("boualem@cse.unsw.edu.au", p.getEmail());
		assertEquals("93854767", p.getContactNumber());
	}
}
