delete from phonebook_record;
delete from users;

-- insert a admin user
insert into users values (1, 'Helen', 'Paik', 1, 'helen', 'helen');
-- insert a general user
insert into users values (2, 'Yun Ki', 'Lee', 2, 'yunki', 'yunki');

-- insert phone book entries

insert into phonebook_record values (1, 1, 'Boualem', 'Boualem Benetallah', 'boualem@cse.unsw.edu.au', '93854767');
insert into phonebook_record values (2, 1, 'Rachid', 'Rachid Hamadi', 'rhamadi@cse.unsw.edu.au', '93856559');

insert into phonebook_record values (3, 2, 'Vincent', 'Chan', 'vincentc@cse.unsw.edu.au', null);
