/*
 * PhonebookServiceImpl.java Created on 12/08/2003
 */
package com.myPhonebook.business.support;

import java.util.List;

import com.myPhonebook.beans.PhonebookBean;
import com.myPhonebook.beans.UserBean;
import com.myPhonebook.business.PhonebookService;
import com.myPhonebook.business.PhonebookServiceException;
import com.myPhonebook.business.UserLoginFailedException;
import com.myPhonebook.dao.DAOFactory;
import com.myPhonebook.dao.DataAccessException;
import com.myPhonebook.dao.PhonebookDAO;
import com.myPhonebook.dao.UserDAO;

/**
 * @author $author
 */
public class PhonebookServiceImpl implements PhonebookService {

	/**
	 * The PhonebookDAO
	 */
	private PhonebookDAO phonebookDao;
	
	/**
	 * The userDAO
	 */
	private UserDAO userDao;

	/**
	 * 
	 */
	public PhonebookServiceImpl() {
		super();
		phonebookDao = DAOFactory.getInstance().getPhonebookDAO();
		userDao = DAOFactory.getInstance().getUserDAO();
	}

	/**
	 * @see com.myPhonebook.business.PhonebookService#login(java.lang.String, java.lang.String)
	 */
	public UserBean login(String username, String password)
		throws UserLoginFailedException {
		UserBean user = null;
		// try find the user using userDao and if it is null, throw 
		// UserLoginFailedException
		
		try {
			user = userDao.findByLoginDetails(username, password);
		} catch (DataAccessException e) {
			throw new UserLoginFailedException("Unable to login", e);
		}
		
		return user;
	}

	/**
	 * @see com.myPhonebook.business.PhonebookService#getPhonebookRecords(int)
	 */
	public List getPhonebookRecords(int userId) throws PhonebookServiceException {
		
		try {
// --fk--
//System.out.println("PhonebookServiceImpl.getPhonebookRecords("+userId+")");
//			List list = phonebookDao.findAllByUser(userId);
//int i;
//PhonebookBean pb;
//
//for (i=0; i<list.size(); i++){
//	pb = (PhonebookBean)list.get(i);
//	System.out.println("PhonebookServiceImpl.getPhonebookRecords("+userId+"): "+pb.getFullName() );
//}
//return list;
// --/--

			return phonebookDao.findAllByUser(userId);
		} catch (DataAccessException e) {
			throw new PhonebookServiceException("Unable to find phonebook records", e);
		}
	}

	/**
	 * @see com.myPhonebook.business.PhonebookService#addRecord(com.myPhonebook.beans.PhonebookBean)
	 */
	public void addRecord(PhonebookBean bean) throws PhonebookServiceException {
		// just call on the insert method of phonebookBean
		try{
			phonebookDao.insert(bean);
		} catch(DataAccessException e){
			throw new PhonebookServiceException("Unable to add record", e);
		}
		
	}

	/**
	 * @see com.myPhonebook.business.PhonebookService#deleteRecord(int)
	 */
	public PhonebookBean deleteRecord(int id) throws PhonebookServiceException {
		// get the phonebookbean from phonebookDao
		// then delete it and finally return it
		PhonebookBean phonebookBean = null;

		try{
			phonebookBean = phonebookDao.getPhonebook(id);
			phonebookDao.delete(id);
		} catch(DataAccessException e){
			throw new PhonebookServiceException("Unable to retreive phonebook bean", e);
		}

		return phonebookBean;
	}

	/**
	 * @see com.myPhonebook.business.PhonebookService#getPhonebook(int)
	 */
	public PhonebookBean getPhonebook(int phoneId) throws PhonebookServiceException {
		// just return the phonebookbean by calling the phonebookDao

		PhonebookBean phonebookBean = null;

		try{
			phonebookBean = phonebookDao.getPhonebook(phoneId);
		} catch(DataAccessException e){
			throw new PhonebookServiceException("Unable to retreive phonebook bean", e);
		}

		return phonebookBean;
	}
}
