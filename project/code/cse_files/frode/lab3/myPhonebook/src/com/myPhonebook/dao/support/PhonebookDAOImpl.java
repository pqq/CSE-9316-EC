/*
 * PhonebookDAOImpl.java
 *
 * Created on 9 August 2003, 14:42
 */

package com.myPhonebook.dao.support;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.myPhonebook.beans.PhonebookBean;
import com.myPhonebook.common.ServiceLocator;
import com.myPhonebook.common.ServiceLocatorException;
import com.myPhonebook.dao.DataAccessException;
import com.myPhonebook.dao.PhonebookDAO;
/**
 * The Data Access Object for phone book records.
 * This object performs the database operations required for maintaining phone
 * book records.
 * @author  yunki
 */
public class PhonebookDAOImpl implements PhonebookDAO {
	
	/**
	 * The service locator to retrieve database connections from
	 */
	private ServiceLocator services;
	
	/** Creates a new instance of PhonebookDAOImpl */
	public PhonebookDAOImpl() {
		try {
			services = ServiceLocator.getInstance();
		} catch (ServiceLocatorException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see com.myPhonebook.dao.PhonebookDAO#insert(com.myPhonebook.beans.PhonebookBean)
	 */
	public void insert(PhonebookBean phonebookBean) throws DataAccessException {
		Connection con = null;
		try {
			con = services.getConnection();
			PreparedStatement stmt = con.prepareStatement("insert into phonebook_record values (phonebook_record_id.nextval, ?, ?, ?, ?, ?)");
			stmt.setInt(1, phonebookBean.getOwner());
			stmt.setString(2, phonebookBean.getShortName());
			stmt.setString(3, phonebookBean.getFullName());
			stmt.setString(4, phonebookBean.getEmail());
			stmt.setString(5, phonebookBean.getContactNumber());
			int n = stmt.executeUpdate();
			if (n != 1)
				throw new DataAccessException("Did not insert one row into database");
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	/**
	 * @see com.myPhonebook.dao.PhonebookDAO#delete(int)
	 */
	public void delete(int id) throws DataAccessException {
		Connection con = null;
		try {
			con = services.getConnection();
			PreparedStatement stmt = con.prepareStatement("delete from phonebook_record where id = ?");
			stmt.setInt(1, id);
			int n = stmt.executeUpdate();
			if (n != 1)
				throw new DataAccessException("Did not delete one row into database");
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}		
	}

	/**
	 * @see com.myPhonebook.dao.PhonebookDAO#findAllForUser(int)
	 */
	public List findAllByUser(int id) throws DataAccessException {
		List list = new ArrayList();
		Connection con = null;
		try {
			con = services.getConnection();
			PreparedStatement stmt = con.prepareStatement("select * from phonebook_record where owner = ?");
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				list.add(createPhonebookBean(rs));
//--fk--
//PhonebookBean pb = (PhonebookBean)list.get((list.size()-1));
//System.out.println("PhonebookDAOImpl.findAllByUser("+id+"): "+pb.getFullName() );
//--/--
			}
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		return list;
	}
	
	/**
	 * Creates and returns a phonebook bean given a ResultSet.
	 * @param rs The result set to retrieve values from. The result set
	 * should already be in the correct position. This method
	 * does not alter rs in any way.
	 * @return An instance of a PhonebookBean which stores the
	 * details in the ResultSet
	 * @throws SQLException When an error occurs while reading the 
	 * database
	 */
	private PhonebookBean createPhonebookBean(ResultSet rs) throws SQLException {
		PhonebookBean phonebook = new PhonebookBean();
		phonebook.setId(rs.getInt("id"));
		phonebook.setOwner(rs.getInt("owner"));
		phonebook.setShortName(rs.getString("short_name"));
		phonebook.setFullName(rs.getString("full_name"));
		phonebook.setEmail(rs.getString("email"));
		phonebook.setContactNumber(rs.getString("contact_number"));
		return phonebook;
	}

	/**
	 * @see com.myPhonebook.dao.PhonebookDAO#getPhonebook(int)
	 */
	public PhonebookBean getPhonebook(int id) throws DataAccessException {

		Connection con = null;
		try {
			con = services.getConnection();
			PreparedStatement stmt = con.prepareStatement("select * from phonebook_record where id = ?");
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				return createPhonebookBean(rs);
			}
		} catch (ServiceLocatorException e) {
			e.printStackTrace();
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		return null;
	}
}
