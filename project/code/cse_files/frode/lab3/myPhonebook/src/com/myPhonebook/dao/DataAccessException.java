/*
 * DataAccessException.java
 * Created on 10/08/2003
 *
 */
package com.myPhonebook.dao;

import com.myPhonebook.common.NestedException;

/**
 * @author yunki
 */
public class DataAccessException extends NestedException {


	/**
	 * @param message
	 */
	public DataAccessException(String message) {
		super(message);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public DataAccessException(String message, Throwable cause) {
		super(message, cause);
	}

}
