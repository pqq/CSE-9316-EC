/*
 * PhonebookDelegateImpl.java Created on 12/08/2003
 * 
 */
package com.myPhonebook.web.helper;

import com.myPhonebook.business.PhonebookService;
import com.myPhonebook.business.support.PhonebookServiceImpl;

/**
 * Simple implementation that just instantiates an instance of
 * the phonebook service - not really idea.
 * @author $author
 */
public class PhonebookDelegateImpl extends PhonebookDelegate {

	private static PhonebookDelegateImpl instance = new PhonebookDelegateImpl();
	
	private PhonebookService service;
	
	private PhonebookDelegateImpl() {
		service = new PhonebookServiceImpl();
	}
	
	public static PhonebookDelegate getInstance() {
		return instance;
	}
	/**
	 * @see com.myPhonebook.web.PhonebookDelegate#getPhonebookService()
	 */
	protected PhonebookService getPhonebookService() {
		return service;
	}

}
