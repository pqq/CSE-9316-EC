/*
 * DeleteCommand.java
 *
 * Created on 9 August 2003, 11:21
 */

package com.myPhonebook.web;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.myPhonebook.web.helper.DelegateFactory;
import com.myPhonebook.web.helper.PhonebookDelegate;
import com.myPhonebook.business.PhonebookServiceException;
import com.myPhonebook.beans.PhonebookBean;

/**
 * This command is to remove a record from the database
 * @author  $author
 */
public class DeleteCommand implements Command {

	/**
	 * The helper class to delegate all function calls to
	 */
	private static PhonebookDelegate phonebookDelegate;
		
	/** Creates a new instance of DeleteCommand */
	public DeleteCommand() {
		// get the phonebookDelegate
		DelegateFactory delegateFactory = DelegateFactory.getInstance();
		phonebookDelegate = delegateFactory.getPhonebookDelegate();
	}
	
	public String execute(HttpServletRequest request, HttpServletResponse response) 
		throws ServletException, IOException {
		
		// to delete following these steps:
		// get the paramater "id" from the request

		int id = Integer.parseInt(request.getParameter("id"));
		PhonebookBean phonebookBean = null;

		// use the id and call on the deleteRecord method 
		// in the phonebookDelegate

		try{
			phonebookBean = phonebookDelegate.deleteRecord(id);
		} catch (PhonebookServiceException e) {
			return "/error.jsp";
		}

		// set an attribute in the request called "entry" which
		// should point to an object of PhonebookBean
		
		request.setAttribute("entry",phonebookBean);
		
		// finally, if everything worked, it should return 
		// the string "/deleted.jsp"
			
		return "/deleted.jsp";
	}
	
}
