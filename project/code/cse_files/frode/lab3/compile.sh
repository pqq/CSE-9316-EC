clear

echo "*** *************** ***"
echo "*** Compiling lab 3 ***"
echo "*** *************** ***"
echo ""

# ------------------------
# setting classpaths
# ------------------------
#export 
#CLASSPATH="/import/elfman/1/cs9316/public_html/03s2/labs/junit3.8.1/junit.jar:." 
CLASSPATH="."
source /home/ecom12/bea/user_projects/myPhonebook/setEnv.sh

echo "*** Classpath is set ***"
#echo CLASSPATH=${CLASSPATH}

# ------------------------
# compiling .java
# ------------------------
cd /home/ecom12/private/frode/lab3/myPhonebook/src
javac -d ../build/WEB-INF/classes com/myPhonebook/beans/*.java
javac -d ../build/WEB-INF/classes com/myPhonebook/business/support/*.java
javac -d ../build/WEB-INF/classes com/myPhonebook/business/*.java
javac -d ../build/WEB-INF/classes com/myPhonebook/common/*.java 
javac -d ../build/WEB-INF/classes com/myPhonebook/dao/support/*.java
javac -d ../build/WEB-INF/classes com/myPhonebook/dao/*.java
javac -d ../build/WEB-INF/classes com/myPhonebook/web/helper/*.java
javac -d ../build/WEB-INF/classes com/myPhonebook/web/*.java

echo "*** Javafiles are compiled ***"

# -----------------------
# compiling JUnit tests
# -----------------------
#cd /home/ecom12/private/frode/lab2/myPhonebook/junit
#javac -classpath ../build/WEB-INF/classes:$CLASSPATH 
#com/myPhonebook/common/ServiceLocatorTest.java 
#javac -classpath ../build/WEB-INF/classes:$CLASSPATH 
#com/myPhonebook/dao/support/PhonebookDAOImplTest.java 


#echo "*** JUnit testfiles are compiled ***"

echo "compilation finished"
date

