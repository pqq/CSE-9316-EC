/*
 * PhonebookDelegate.java Created on 12/08/2003
 * 
 */
package com.myPhonebook.web.helper;

import java.util.List;

import com.myPhonebook.beans.UserBean;
import com.myPhonebook.beans.PhonebookBean;
import com.myPhonebook.business.PhonebookService;
import com.myPhonebook.business.PhonebookServiceException;
import com.myPhonebook.business.UserLoginFailedException;

/**
 * Abstract business delegate. This class implements all the methods
 * that required by leaves the method getPhonebookService() of the class to 
 * subclasses. 
 * 
 * @author $author
 */
public abstract class PhonebookDelegate {

    /**
     * Find the phonebook service.
     * @return The business interface PhoneBookService
     */
	protected abstract PhonebookService getPhonebookService();

	private PhonebookService phonebookService;
	
	/**
	 * Finds a user with the given details
	 * @param username The username to login with
	 * @param password The password to login with
	 * @return A user if one exists with the given details
	 * @throws UserLoginFailedException When no such user is found
	 */
	public UserBean login(String username, String password) throws UserLoginFailedException {
		return getPhonebookService().login(username, password);
	}
	
	/**
	 * Returns all the phonebooks for a particular user
	 * @param userId
	 * @return A list of all phonebook records
	 * @throws PhonebookServiceException When an error occurs
	 */
	public List getPhonebookRecords(int userId) throws PhonebookServiceException {
// --fk--
//System.out.println("PhonebookDelegate.getPhonebookRecords("+userId+")");
// --/--
		return getPhonebookService().getPhonebookRecords(userId);
	}

	/**
	 * Adds a record to the phonebook
	 * @param bean The phone book record to add
	 * @throws PhonebookServiceException When an error occurs
	 */
	// TODO method to add a phonebook record
	public void addRecord(PhonebookBean bean) throws PhonebookServiceException {
		getPhonebookService().addRecord(bean);
	}

	/**
	 * Removes a phonebook record from the database
	 * @param id The id of the record to delete
	 * @throws PhonebookServiceException When an error occurs
	 */
	// TODO method to delete record
	public PhonebookBean deleteRecord(int id) throws PhonebookServiceException {
		return getPhonebookService().deleteRecord(id);
	}	
}
