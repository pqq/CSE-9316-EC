/*
 * PhonebookServiceException.java Created on 12/08/2003
 * 
 */
package com.myPhonebook.business;

import com.myPhonebook.common.NestedException;

/**
 * @author yunki
 */
public class PhonebookServiceException extends NestedException {

	/**
	 * @param message
	 */
	public PhonebookServiceException(String message) {
		super(message);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public PhonebookServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param cause
	 */
	public PhonebookServiceException(Throwable cause) {
		super(cause);
	}

}
