/*
 * PhonebookService.java Created on 12/08/2003
 * 
 */
package com.myPhonebook.business;

import java.util.List;

import com.myPhonebook.beans.PhonebookBean;
import com.myPhonebook.beans.UserBean;

/**
 * @author $author
 */
public interface PhonebookService {

	/**
	 * Finds a user with the given details
	 * @param username The username to login with
	 * @param password The password to login with
	 * @return A user if one exists with the given details
	 * @throws UserLoginFailedException When no such user is found
	 */
	UserBean login(String username, String password) throws UserLoginFailedException;
	
	/**
	 * Returns all the phonebooks for a particular user
	 * @param userId
	 * @return A list of all phonebook records
	 * @throws PhonebookServiceException When an error occurs
	 */
	List getPhonebookRecords(int userId) throws PhonebookServiceException;
	/**
	 * Adds a record to the phonebook
	 * @param bean The phone book record to add
	 * @throws PhonebookServiceException When an error occurs
	 */
	void addRecord(PhonebookBean bean) throws PhonebookServiceException;
	/**
	 * Removes a phonebook record from the database
	 * @param id The id of the record to delete
	 * @return The phonebook that was deleted from the database
	 * @throws PhonebookServiceException When an error occurs
	 */
	PhonebookBean deleteRecord(int id) throws PhonebookServiceException;

}
