/*
 * ListCommand.java
 *
 * Created on 9 August 2003, 11:46
 */

package com.myPhonebook.web;

import java.io.*;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.myPhonebook.beans.UserBean;
import com.myPhonebook.business.PhonebookServiceException;
import com.myPhonebook.web.helper.DelegateFactory;
import com.myPhonebook.web.helper.PhonebookDelegate;


import com.myPhonebook.beans.PhonebookBean; // just for testing purpose

/**
 * This command finds all the phone book entries for a particular user
 * unless they are admin
 * @author  yunki
 */
public class ListCommand implements Command {
	/**
	 * The helper class to delegate all function calls to
	 */
	private static PhonebookDelegate phonebookDelegate;
	/** Creates a new instance of ListCommand */
	public ListCommand() {
		phonebookDelegate = phonebookDelegate = DelegateFactory.getInstance().getPhonebookDelegate();			
	}
	
	public String execute(HttpServletRequest request, HttpServletResponse response) 
		throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		UserBean user = (UserBean) session.getAttribute("user");
		int id = user.getId();
		
		try {
			List list = phonebookDelegate.getPhonebookRecords(id);
			request.setAttribute("list", list);


// --fk--
/*
int i;
PhonebookBean pb;

for (i=0; i<list.size(); i++){
	pb = (PhonebookBean)list.get(i);
	System.out.println("ListCommand.execute(): ("+id+"): "+pb.getFullName() );
}


System.out.println("ListCommand.execute()");
System.out.println("ListCommand.execute(): user.getFirstname(): "+user.getFirstname());
System.out.println("ListCommand.execute(): user.getLastname(): "+user.getLastname());
*/
// --/--						

			return "/home.jsp";
		} catch (PhonebookServiceException e) {
			e.printStackTrace();
		}	
		return "/error.jsp";
	}
	
}
