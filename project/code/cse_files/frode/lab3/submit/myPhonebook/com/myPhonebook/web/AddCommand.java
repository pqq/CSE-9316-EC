/*
 * AddCommand.java
 *
 * Created on 9 August 2003, 11:20
 */

package com.myPhonebook.web;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.myPhonebook.web.helper.PhonebookDelegate;
import com.myPhonebook.web.helper.DelegateFactory;
import com.myPhonebook.beans.PhonebookBean;
import com.myPhonebook.beans.UserBean;
import com.myPhonebook.business.PhonebookServiceException;

/**
 * This command adds a new phone book entry
 * @author $author 
 */
public class AddCommand implements Command {
	/**
	 * The helper class to delegate all function calls to
	 */
	private static PhonebookDelegate phonebookDelegate;
	
	/** Creates a new instance of AddCommand */
	public AddCommand() {
		// TODO get the delegate
		DelegateFactory delegateFactory = DelegateFactory.getInstance();
		phonebookDelegate = delegateFactory.getPhonebookDelegate();
	}
	
	public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// To add a phonebook record, you will need to:
		// get the current user from the HttpSession, 
		// their id will be the "owner" of the new record

		HttpSession session = request.getSession();
		UserBean user = (UserBean) session.getAttribute("user");
		int id = user.getId();
		
		// then get from the request the following parameters:
		// phone, email, completeName and shortName
		// and using all these values, populate the properties
		// of a PhonebookBean

		PhonebookBean phonebookBean = new PhonebookBean();

		phonebookBean.setOwner(id);
		phonebookBean.setContactNumber(request.getParameter("phone"));
		phonebookBean.setEmail(request.getParameter("email"));
		phonebookBean.setFullName(request.getParameter("completeName"));
		phonebookBean.setShortName(request.getParameter("shortName"));

//--fk--
//System.out.println("AddCommand.execute(): "+id+" - "+phonebookBean.getEmail());
//--/--

		// then call on the addRecord(phonebookBean) on the phonebookDelegate

		try{
			phonebookDelegate.addRecord(phonebookBean);
		} catch (PhonebookServiceException e) {
			//throw new ServletException("Unable to add phonebook record; "+ e.getMessage(), e);
			return "/error.jsp";
		}

		// and finally set the attribute "shortName" to be
		// the shortName of the bean
		// the page to return if all this is successful is added.jsp
		
		request.setAttribute("shortName", request.getParameter("shortName"));
		
		return "/added.jsp";
	}
	
}
