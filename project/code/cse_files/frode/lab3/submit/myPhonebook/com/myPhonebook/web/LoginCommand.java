/*
 * LoginCommand.java
 *
 * Created on 9 August 2003, 11:12
 */

package com.myPhonebook.web;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.myPhonebook.beans.UserBean;
import com.myPhonebook.business.UserLoginFailedException;
import com.myPhonebook.web.helper.DelegateFactory;
import com.myPhonebook.web.helper.PhonebookDelegate;

/**
 * This is the command that will be used for logging in users.
 * If logon is successful, the command should place a list of phonebook entries
 * in the request attriubutes.
 * @author  yunki
 */
public class LoginCommand implements Command {
	/**
	 * The helper class to delegate all function calls to
	 */
	private static PhonebookDelegate phonebookDelegate;	
	/** Creates a new instance of LoginCommand */
	public LoginCommand() {
		phonebookDelegate = phonebookDelegate = DelegateFactory.getInstance().getPhonebookDelegate();
	}
	
	public String execute(HttpServletRequest request, HttpServletResponse response) 
	throws ServletException, IOException {
	
		try {
			UserBean user = phonebookDelegate.login(
				request.getParameter("username"), request.getParameter("password"));

// -- fk --
//System.out.println("LoginCommand(): username: "+request.getParameter("username")+" password: "+request.getParameter("password"));
//System.out.println("LoginCommand(): user.getFirstname(): "+user.getFirstname());
//System.out.println("LoginCommand(): user.getLastname(): "+user.getLastname());
// -- / --
				
			if (user == null) {
				return "/index.jsp";
			}
			HttpSession session = request.getSession();
			session.setAttribute("user", user);

			// this is not a jsp so it will chain the commands together
			return "list";
		} catch (UserLoginFailedException e) {
			e.printStackTrace();
			return "/loginfailed.jsp";
		}
	}
	
}
