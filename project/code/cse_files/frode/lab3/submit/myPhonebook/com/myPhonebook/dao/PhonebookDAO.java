/*
 * PhonebookDAO.java
 *
 * Created on 9 August 2003, 14:39
 */

package com.myPhonebook.dao;

import java.util.List;

import com.myPhonebook.beans.PhonebookBean;

/**
 * The Data Access Object for phonebook entries
 * @author  yunki
 */
public interface PhonebookDAO {
	
	/**
	 * Inserts a new phone book in the database
	 * 
	 * @param phonebookBean The entry to insert
	 * @throws DataAccessException
	 */
	void insert(PhonebookBean phonebookBean) throws DataAccessException;
	
	/**
	 * Removes an entry from the database
	 * @param id The id of the database to remove
	 * @throws DataAccessException When error occurs while connecting
	 * to database
	 */
	void delete(int id) throws DataAccessException;
	
	/**
	 * Finds all the phone book entries for a particular user
	 * @param id The id of the user to find
	 * @return A list of all the phonebookbeans owned by this user
	 * @throws DataAccessException When error occurs whle connecting 
	 * to the database
	 */
	List findAllByUser(int id) throws DataAccessException;
	
	PhonebookBean getPhonebook(int id) throws DataAccessException;
}
