/*
 * UserDAOImpl.java
 *
 * Created on 9 August 2003, 14:41
 */

package com.myPhonebook.dao.support;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.myPhonebook.beans.UserBean;
import com.myPhonebook.common.ServiceLocator;
import com.myPhonebook.common.ServiceLocatorException;
import com.myPhonebook.dao.DataAccessException;
import com.myPhonebook.dao.UserDAO;
/**
 * The Data Access Object for users.
 * 
 * @author  yunki
 */
public class UserDAOImpl implements UserDAO {
	
	private static ServiceLocator services;
	
	/** Creates a new instance of UserDAOImpl */
	public UserDAOImpl() {
		try {
			services = ServiceLocator.getInstance();
		} catch (ServiceLocatorException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see com.myPhonebook.dao.UserDAO#findByLoginDetails(java.lang.String, java.lang.String)
	 */
	public UserBean findByLoginDetails(String username, String password) throws DataAccessException {
		Connection con = null;
		try {
			con = services.getConnection();
			PreparedStatement stmt = con.prepareStatement("select * from users where username = ? and password = ?");
			stmt.setString(1, username);
			stmt.setString(2, password);
			ResultSet rs = stmt.executeQuery();
			if (rs.next())
				return createUserBean(rs);
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		return null;
	}
	
	/**
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private UserBean createUserBean(ResultSet rs) throws SQLException {
		UserBean user = new UserBean();
		user.setId(rs.getInt("id"));
		user.setFirstname(rs.getString("firstname"));
		user.setLastname(rs.getString("lastname"));
		user.setAccessLevel(rs.getInt("access_level"));
		user.setUsername(rs.getString("username"));
		user.setPassword(rs.getString("password"));
		return user;
	}
}
