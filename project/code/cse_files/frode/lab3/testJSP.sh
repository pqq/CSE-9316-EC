clear
 
echo "*** *************** ***"
echo "*** Testing JSP     ***"
echo "*** *************** ***"
echo ""

 
 
# ------------------------
# setting classpaths 
# ------------------------
CLASSPATH="." 
source /home/ecom12/bea/user_projects/myPhonebook/setEnv.sh
 
echo "*** Classpath is set ***"


# ------------------------
# copy JSP files
# ------------------------
cp /home/ecom12/private/frode/lab3/myPhonebook/web/*.jsp /home/ecom12/private/frode/lab3/myPhonebook/build/
cp /home/ecom12/private/frode/lab3/myPhonebook/web/WEB-INF/* /home/ecom12/private/frode/lab3/myPhonebook/build/WEB-INF/
 
echo "*** JSP files are copied into build ***"
 
 
# ------------------------
# testing JSP files 
# ------------------------ 
cd /home/ecom12/private/frode/lab3/myPhonebook/build/
#java weblogic.jspc added.jsp
#java weblogic.jspc deleted.jsp
#java weblogic.jspc error.jsp
#java weblogic.jspc index.jsp
#java weblogic.jspc loginfailed.jsp
java weblogic.jspc home.jsp

ls -ltr /home/ecom12/private/frode/lab3/myPhonebook/build/*.class

echo "*** JSP files are compiled ***";

