clear
 
echo "*** *************** ***"
echo "*** Deploying lab 3 ***"
echo "*** *************** ***"
echo ""

 
# ------------------------
# setting classpaths
# ------------------------
CLASSPATH="."
source /home/ecom12/bea/user_projects/myPhonebook/setEnv.sh
 
echo "*** Classpath is set ***"

 
# ------------------------
# copy JSP files
# ------------------------
cp /home/ecom12/private/frode/lab3/myPhonebook/web/*.jsp /home/ecom12/private/frode/lab3/myPhonebook/build/
cp /home/ecom12/private/frode/lab3/myPhonebook/web/WEB-INF/* /home/ecom12/private/frode/lab3/myPhonebook/build/WEB-INF/

echo "*** JSP files are copied into build ***"


# -----------------------
# creating a .war file
# -----------------------
cd /home/ecom12/private/frode/lab3/myPhonebook/build/
jar -cf /home/ecom12/private/frode/lab3/myPhonebook.war *
 
echo "*** myPhonebook.war is created ***"


# -----------------------
# auto deployment
# -----------------------
cp /home/ecom12/private/frode/lab3/myPhonebook.war /home/ecom12/bea/user_projects/myPhonebook/applications/

echo "*** myPhonebook.war is deployed ***";
