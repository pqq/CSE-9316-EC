<%@ page contentType="text/html" import="com.freePowder.beans.*,java.util.*, 
                                         com.freePowder.web.helper.DelegateFactory,
										 com.freePowder.web.helper.ListingDelegate,
										 import com.freePowder.business.ListingServiceException"%>
										 
<% ListingDelegate listingDelegate;
   listingDelegate = DelegateFactory.getInstance().getListingDelegate();
   
   try {
   		List productlist = listingDelegate.getProducts();
		request.setAttribute("productlist", productlist);
   } 
   catch (ListingServiceException e) {
		e.printStackTrace();
   }
   
%>
<jsp:useBean id="entry" class="com.freePowder.beans.BrowseBean"/>
<jsp:useBean id="productlist" class="java.util.ArrayList" scope="request"/>

<% 
int i = Integer.parseInt(request.getParameter("id"));
entry = (BrowseBean) productlist.get(i); 
%>

<!-- Start Product Section-->
<table width="100%"  border="0" cellspacing="5" cellpadding="0">
  <tr>
    <td width="80"><img src="images/spacer.gif" width="80" height="30"></td>
    <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#EBF1FB">
      <tr>
        <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="233"><img src="images/product_01.gif" width="233" height="79"></td>
              <td background="images/product_02.gif"><img src="images/product_02.gif" width="10" height="79"></td>
              <td width="207"><img src="images/product_03.jpg" width="207" height="79"></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td>
			<table width="100%"  border="0" cellspacing="3" cellpadding="2">
              <tr>
                <td width="279" rowspan="5" align="center" valign="middle" bgcolor="#FFFFFF"><img src="products/<jsp:getProperty name="entry" property="productname"/>.jpg"></td>
                <td bgcolor="#FFFFFF"><b>Product Name:</b></td>
                <td><jsp:getProperty name="entry" property="productname"/></td>
              </tr>
              <tr>
                <td bgcolor="#FFFFFF"><b>Brand:</b></td>
                <td><jsp:getProperty name="entry" property="brandname"/></td>
              </tr>
              <tr>
                <td bgcolor="#FFFFFF"><b>Category:</b></td>
                <td>
				<jsp:getProperty name="entry" property="cat_lev1"/> :: 
			    <jsp:getProperty name="entry" property="cat_lev2"/> :: 
			    <jsp:getProperty name="entry" property="cat_lev3"/>
				</td>
              </tr>
              <tr>
                <td bgcolor="#FFFFFF"><b>Price:</b></td>
                <td>$<jsp:getProperty name="entry" property="price"/>.00</td>
              </tr>
              <tr>
                <td bgcolor="#FEFEFE"><b>Number in stock:</b></td>
                <td><jsp:getProperty name="entry" property="no_instock"/></td>
              </tr>
              <tr align="right" bgcolor="#F4BE00">
                <td colspan="3"><form action="/freePowder/" method="post">
				<input name="product_id" type="hidden" id="product_id" value="product_id">
				<input type="submit" value="Add to Cart" class="button-menu">
				</form></td>
              </tr>
            </table>		</td>
      </tr>
    </table>
    </td>
  </tr>
</table>
<!-- End Product Section-->