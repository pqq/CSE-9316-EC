<%@ page contentType="text/html" import="com.freePowder.beans.*,java.util.*"%>
<jsp:useBean id="entry" class="com.freePowder.beans.SupplierBean"/>
<jsp:useBean id="supplierslist" class="java.util.ArrayList" scope="request"/>
<html>
<h2>List suppliers</h2>
<table width="100%" border=0>
<tr>
	<td><b>suppliername</b></td>
	<td><b>xml_url</b></td>
	<td><b>update_url</b></td>
</tr>

<% for (int i = 0; i < supplierslist.size(); i++) { 
	entry = (SupplierBean) supplierslist.get(i);
%>
	<tr>
		<td><jsp:getProperty name="entry" property="suppliername"/></td>
		<td><jsp:getProperty name="entry" property="xml_url"/></td>
		<td><jsp:getProperty name="entry" property="update_url"/></td>
	</tr>
<% } %>
</table>

</html>
