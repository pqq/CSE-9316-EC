<%@ page contentType="text/html" import="com.freePowder.beans.*,java.util.*, 
                                         com.freePowder.web.helper.DelegateFactory,
										 com.freePowder.web.helper.ListingDelegate,
										 import com.freePowder.business.ListingServiceException"%>
										 
<% int maxNews = 1;

   ListingDelegate listingDelegate;
   listingDelegate = DelegateFactory.getInstance().getListingDelegate();
   
   try {
   		List list1 = listingDelegate.getNews();
		request.setAttribute("newslist", list1);
   } 
   catch (ListingServiceException e) {
		e.printStackTrace();
   }
%>
<jsp:useBean id="entry" class="com.freePowder.beans.NewsBean"/>
<jsp:useBean id="newslist" class="java.util.ArrayList" scope="request"/>
<table width="100%"  border="0" cellspacing="5" cellpadding="0">
  <tr>
    <td width="80"><img src="images/spacer.gif" width="80" height="30"></td>
    <td valign="top" bgcolor="#EBF1FB"><table width="100%"  border="0" cellspacing="3" cellpadding="2">
      <tr bgcolor="#4A6077">
        <td colspan="4"><div class="adminHead">News Items</div> </td>
        </tr>
      <tr bgcolor="#FFFFFF">
        <td><b>News Number</b> </td>
        <td><b>Heading</b></td>
        <td><b>Date</b></td>
        <td><b>Delete</b></td>
      </tr>
	  <% for (int i = 0; i < newslist.size(); i++) { 
			entry = (NewsBean) newslist.get(i); 
			maxNews++; %>
      <tr>
        <td><jsp:getProperty name="entry" property="news_pk"/></td>
        <td><jsp:getProperty name="entry" property="heading"/></td>
        <td><jsp:getProperty name="entry" property="timestamp"/></td>
        <td align="left">
			<form action="/freePowder/" method="post">
			<input name="news_pk" type="hidden" id="<jsp:getProperty name="entry" property="news_pk"/>" value="<jsp:getProperty name="entry" property="news_pk"/>">
			<input type="submit" value="Delete">
		    </form>		</td>
      </tr>
	  <% } %>
      <tr bgcolor="#F4BE00">
        <td colspan="4"><img src="images/spacer.gif" width="200" height="1"></td>
      </tr>
	  <tr bgcolor="#4A6077">
        <td colspan="4"><div class="adminHead">Create New News Item</div></td>
      </tr>
	  <tr bgcolor="#FFFFFF">
        <td colspan="2"><b>Heading</b></td>
        <td><b>Date</b></td>
        <td>&nbsp;</td>
      </tr>
	  <form action="/freePowder/dispatcher?operation=addnews" method="post">
	  <tr>
        <td colspan="2"><input name="heading" type="text"></td>
        <td><input name="timestamp" type="text"></td>
        <td>&nbsp;</td>
      </tr>
	  <tr bgcolor="#FFFFFF">
        <td colspan="4"><b>Body</b></td>
      </tr>
	  <tr>
        <td colspan="4">
			<textarea name="body" cols="70" rows="10" wrap="VIRTUAL"></textarea>
		</td>
      </tr>
	  <tr bgcolor="#F4BE00"><td colspan="4" align="right"><input type="submit" value="Add News"></td></tr>
	  </form>
    </table></td>
  </tr>
</table>