<%@ page contentType="text/html" import="com.freePowder.beans.*,java.util.*, 
                                         com.freePowder.web.helper.DelegateFactory,
										 com.freePowder.web.helper.ListingDelegate,
										 import com.freePowder.business.ListingServiceException"%>
										 
<% ListingDelegate listingDelegate;
   listingDelegate = DelegateFactory.getInstance().getListingDelegate();
   
   try {
   		List productlist = listingDelegate.getProducts();
		request.setAttribute("productlist", productlist);
   } 
   catch (ListingServiceException e) {
		e.printStackTrace();
   }
   
%>


<jsp:useBean id="entry" class="com.freePowder.beans.BrowseBean"/>
<jsp:useBean id="productlist" class="java.util.ArrayList" scope="request"/>

<html>
<h2>List products</h2>
<table width="100%" border=1>
<tr><th>product_pk</th><th>brandname</th><th>productname</th><th>cat_lev1</th><th>cat_lev2</th><th>cat_lev3</th><th>no_instock</th><th>price</th></tr>

<% for (int i = 0; i < productlist.size(); i++) { 
	entry = (BrowseBean) productlist.get(i);
%>
	<tr>
		<td><jsp:getProperty name="entry" property="product_pk"/></td>
		<td><jsp:getProperty name="entry" property="brandname"/></td>
		<td><jsp:getProperty name="entry" property="productname"/></td>
		<td><jsp:getProperty name="entry" property="cat_lev1"/></td>
		<td><jsp:getProperty name="entry" property="cat_lev2"/></td>
		<td><jsp:getProperty name="entry" property="cat_lev3"/></td>
		<td><jsp:getProperty name="entry" property="no_instock"/></td>
		<td><jsp:getProperty name="entry" property="price"/></td>
	</tr>
<% } %>
</table>

</html>
