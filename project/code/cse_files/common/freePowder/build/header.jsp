<!-- Start Main Logo Area -->
<table width=100% border=0 cellpadding=0 cellspacing=0>
  <tr> 
    <td width="112" rowspan=2 bgcolor=#415464> <img src="images/logo-01.gif" width=112 height=131 alt="" align="top"></td>
    <td width="190" rowspan=2 bgcolor=#415464> <img src="images/logo-02.gif" width=190 height=131 alt="" align="top"></td>
    <td width="125" bgcolor=#FFFFFF> <a href="index.jsp?page=contact"><img src="images/logo-contact-us.gif" alt="" width=125 height=36 border="0" align="top"></a></td>
    <td width="82" bgcolor=#FFFFFF> <a href="index.jsp?page=home"><img src="images/logo-home.gif" alt="" width=82 height=36 border="0" align="top"></a></td>
    <td width="117" bgcolor=#FFFFFF> <a href="index.jsp?page=mydetails"><img src="images/logo-my-details.gif" alt="" width=117 height=36 border="0" align="top"></a></td>
    <td width="167" bgcolor=#FFFFFF> <a href="index.jsp?page=browse"><img src="images/browse-products.gif" alt="" width=167 height=36 border="0" align="top"></a></td>
    <td background="images/logo-06.gif"> <img src="images/logo-06.gif" width=7 height=36 alt="" align="top"></td>
  </tr>
  <tr> 
    <td width="125" bgcolor=#415464> <img src="images/logo-03.gif" width=125 height=95 alt="" align="top"></td>
    <td width="82" bgcolor=#415464> <img src="images/logo-04.gif" width=82 height=95 alt="" align="top"></td>
    <td colspan=2 bgcolor=#415464> <img src="images/logo-05.gif" width=284 height=95 alt="" align="top"></td>
    <td background="images/logo-07.gif"> <img src="images/logo-07.gif" width=7 height=95 alt="" align="top"></td>
  </tr>
</table>
<!-- End Main Logo Area -->

<% if(session.getAttribute("user") == null) { %>
	<!-- Start Navigation Area (user not logged in) -->
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
	  <tr>
		<td width="28" bgcolor=#D8A900> <img src="images/menu-01.gif" width=28 height=36 alt="" align="top"></td>
		<td width="140" bgcolor=#D8A900> <img src="images/menu-product-browser.gif" width=140 height=36 alt="" align="top"></td>
		<form name="select" method="post" action="">
		  <td background="images/menu-02.gif" bgcolor=#D8A900><img src="images/menu-02.gif" width="2" height="36" align="top"> 
			<select name="browse" class="textfield">
			  <option selected>Browse Products</option>
			  <option>-------------------</option>
			  <option>Brand Name</option>
			  <option>---Rossignol</option>
			  <option>---Salomon</option>
			  <option>Product Category</option>
			  <option>---Skis</option>
			  <option>---Boots</option>
			</select>
			<input type="submit" name="browse-button" value="Browse" class="button-menu">
		  </td>
		</form>
		<td width="45" bgcolor=#D8A900> <img src="images/menu-login.gif" alt="" width=45 height=36 align="top"></td>
		<form action="/freePowder/dispatcher" method="post">
		  <td width="335" background="images/menu-welcome-offline.gif" bgcolor=#D8A900> 
			 <input type="hidden" name="operation" value="login">
			 <input name="loginname" type="text" class="textfield" size="10" maxlength="16">
			 <img src="images/menu-password.gif" width="88" height="36" align="top"> 
			 <input name="password" type="password" class="textfield" size="10" maxlength="16">
			 <input name="login" type="submit" class="textfield" value="GO">
		  </td>
		</form>
		<td width="7" bgcolor=#D8A900> <img src="images/menu-04.gif" width=7 height=36 alt="" align="top"></td>
	  </tr>
	  <tr> 
		<td width="28" bgcolor=#D8A900> <img src="images/menu-05.gif" width=28 height=34 alt="" align="top"></td>
		<td width="140" bgcolor=#D8A900> <img src="images/menu-keyword.gif" width=140 height=34 alt="" align="top"></td>
		<form name="search" method="post" action="">
		  <td background="images/menu-06.gif" bgcolor=#D8A900><img src="images/menu-06.gif" width="2" height="34" align="top"> 
			<input type="text" class="textfield" name="textfield" size="19" maxlength="32">
			<input type="submit" name="search-button" value="Search" class="button-menu">
		  </td>
		</form>
		<td colspan="2" bgcolor=#D8A900><a href="index.jsp?page=newuser"><img src="images/menu-user-create.gif" width="293" height="34" border="0" align="top"></a> </td>
		<td width="7" bgcolor=#D8A900> <img src="images/menu-08.gif" width=7 height=34 alt="" align="top"></td>
	  </tr>
	  <tr> 
		<td width="28" bgcolor=#FFFFFF> <img src="images/menu-09.gif" width=28 height=6 alt="" align="top"></td>
		<td width="140" bgcolor=#FFFFFF> <img src="images/menu-10.gif" width=140 height=6 alt="" align="top"></td>
		<td background="images/menu-11.gif" bgcolor=#D8A900> <img src="images/menu-11.gif" width=287 height=6 alt="" align="top"></td>
		<td colspan="2" bgcolor=#D8A900>  <img src="images/menu-13.gif" width=380 height=6 alt="" align="top"></td>
		<td width="7" bgcolor=#D8A900> <img src="images/menu-14.gif" width=7 height=6 alt="" align="top"></td>
	  </tr>
	</table>
	<!-- End Navigation Area (user not logged in) -->
<%} else {%>
<jsp:useBean id="user" type="com.freePowder.beans.UsrBean" scope="session"/>
<!-- Start Navigation Area (user logged in) -->
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
	  <tr> 
		<td width="28" bgcolor=#D8A900> <img src="images/menu-01.gif" width=28 height=36 alt="" align="top"></td>
		<td width="140" bgcolor=#D8A900> <img src="images/menu-product-browser.gif" width=140 height=36 alt="" align="top"></td>
		<form name="login" method="post" action="">
		  <td background="images/menu-02.gif" bgcolor=#D8A900><img src="images/menu-02.gif" width="2" height="36" align="top"> 
			<select name="browse" class="textfield">
			  <option selected>Browse Products</option>
			  <option>-------------------</option>
			  <option>Brand Name</option>
			  <option>---Rossignol</option>
			  <option>---Salomon</option>
			  <option>Product Category</option>
			  <option>---Skis</option>
			  <option>---Boots</option>
			</select>
			<input type="submit" name="browse-button" value="Browse" class="button-menu">
		  </td>
		</form>
		<td width="45" bgcolor=#D8A900> <img src="images/menu-login-online.gif" width=45 height=36 alt="" align="top"></td>
		<td background="images/menu-welcome-offline.gif" bgcolor=#D8A900>
		<form action="/freePowder/dispatcher" method="post">
            <input type="hidden" name="operation" value="logout">
			<input type="image" src="images/menu-logout.gif"  width="120" height="36" border="0" alt="Logout">
        </form></td>
		<td width="7" bgcolor=#D8A900> <img src="images/menu-04.gif" width=7 height=36 alt="" align="top"></td>
	  </tr>
	  <tr> 
		<td width="28" bgcolor=#D8A900> <img src="images/menu-05.gif" width=28 height=34 alt="" align="top"></td>
		<td width="140" bgcolor=#D8A900> <img src="images/menu-keyword.gif" width=140 height=34 alt="" align="top"></td>
		<form name="search" method="post" action="">
		  <td background="images/menu-06.gif" bgcolor=#D8A900><img src="images/menu-06.gif" width="2" height="34" align="top"> 
			<input type="text" class="textfield" name="textfield" size="19" maxlength="32">
			<input type="submit" name="search-button" value="Search" class="button-menu">
		  </td>
		</form>
		<td width="45" bgcolor=#D8A900> <img src="images/spacer.gif" alt="" width=45 height=34 align="top"></td>
		<td background="images/menu-user-online.gif" bgcolor=#F8C200>
		<img src="images/spacer.gif" alt="" width=6 height=30 align="top">
		 <% 
		   String userName = user.getFirst_name();
		   userName = userName.concat(" ");
		   userName = userName.concat(user.getLast_name());
		   
		   char[] nameArray = userName.toCharArray();
		   
		   for(int i = 0; i < nameArray.length; i++) {
		       if(nameArray[i] == ' ')
			       out.print("<img src=\"images/spacer.gif\" width=4 height=30 align=\"top\">");
			   else if(nameArray[i] == '.')
			       out.print("<img src=\"letters/dot.gif\" height=30 align=\"top\">");
			   else if(nameArray[i] == ',')
			       out.print("<img src=\"letters/comma.gif\" height=30 align=\"top\">");
			   else if(nameArray[i] == '-')
			       out.print("<img src=\"letters/hyphen.gif\" height=30 align=\"top\">");
			   else
			       out.print("<img src=\"letters/"+ Character.toLowerCase(nameArray[i]) +".gif\" height=30 align=\"top\">");
		   }
		%>
		</td>
		<td width="7" bgcolor=#F8C200> <img src="images/spacer.gif" width=7 height=34 alt="" align="top"></td>
	  </tr>
	  <tr> 
		<td width="28" bgcolor=#FFFFFF> <img src="images/menu-09.gif" width=28 height=6 alt="" align="top"></td>
		<td width="140" bgcolor=#FFFFFF> <img src="images/menu-10.gif" width=140 height=6 alt="" align="top"></td>
		<td background="images/menu-11.gif" bgcolor=#D8A900> <img src="images/menu-11.gif" width=287 height=6 alt="" align="top"></td>
		<td width="45" bgcolor=#D8A900> <img src="images/menu-12.gif" width=45 height=6 alt="" align="top"></td>
		<td width="293" background="images/menu-13.gif" bgcolor=#D8A900> <img src="images/menu-13.gif" width=293 height=6 alt="" align="top"></td>
		<td width="7" bgcolor=#D8A900> <img src="images/menu-14.gif" width=7 height=6 alt="" align="top"></td>
	  </tr>
	</table>
	<!-- End Navigation Area (user logged in) -->
<% } %>