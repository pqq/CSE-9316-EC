<%@ page contentType="text/html" import="com.freePowder.beans.*,java.util.*, 
                                         com.freePowder.web.helper.DelegateFactory,
										 com.freePowder.web.helper.AdminDelegate,
										 import com.freePowder.business.AdminServiceException"%>
										 
<% AdminDelegate adminDelegate;
   adminDelegate = DelegateFactory.getInstance().getAdminDelegate();
   
   try {
   		List list1 = adminDelegate.getSuppliers();
		request.setAttribute("supplierslist", list1);
   } 
   catch (AdminServiceException e) {
		e.printStackTrace();
   }
%>
										 
<jsp:useBean id="entry" class="com.freePowder.beans.SupplierBean"/>
<jsp:useBean id="supplierslist" class="java.util.ArrayList" scope="request"/>
<table width="100%"  border="0" cellspacing="5" cellpadding="0">
  <tr>
    <td width="80"><img src="images/spacer.gif" width="80" height="30"></td>
    <td valign="top" bgcolor="#EBF1FB"><table width="100%"  border="0" cellspacing="3" cellpadding="2">
      <tr bgcolor="#4A6077">
        <td colspan="5"><div class="adminHead">Suppliers</div> </td>
        </tr>
      <tr>
		 <td><b>Suppliername</b></td>
		 <td><b>xml_url</b></td>
		 <td><b>update_url</b></td>
		 <td>&nbsp;</td>
		 <td>&nbsp;</td>
	 </tr>
	 <% for (int i = 0; i < supplierslist.size(); i++) { 
		 entry = (SupplierBean) supplierslist.get(i);
	 %>
	 <form action="">
	  <tr>
		 <td><input type="text" name="suppliername" value="<jsp:getProperty name="entry" property="suppliername"/>" size="30"></td>
		 <td><input type="text" name="xml_url" value="<jsp:getProperty name="entry" property="xml_url"/>" size="30"></td>
		 <td><input type="text" name="update_url" value="<jsp:getProperty name="entry" property="update_url"/>" size="30"></td>
		 <td><a href="download_xml.jsp?xml_url=<jsp:getProperty name="entry" property="xml_url"/>">downloadxml</a></td>
		 <td><input type="submit" name="" value="update"></td>
	 </tr>
	 </form>
	 <% } %>
      <tr bgcolor="#F4BE00">
        <td colspan="4"><img src="images/spacer.gif" width="200" height="1"></td>
      </tr>
	  
	  
    </table></td>
  </tr>
</table>