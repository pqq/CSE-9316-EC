<%@ page contentType="text/html" import="com.freePowder.beans.*,java.util.*"%>
<jsp:useBean id="entry" class="com.freePowder.beans.NewsBean"/>
<jsp:useBean id="newslist" class="java.util.ArrayList" scope="request"/>
<html>
<h2>List news</h2>
<table width="100%" border=1>
<tr><th>heading</th><th>body</th></tr>

<% for (int i = 0; i < newslist.size(); i++) { 
	entry = (NewsBean) newslist.get(i);
%>
	<tr>
		<td><jsp:getProperty name="entry" property="heading"/></td>
		<Td><jsp:getProperty name="entry" property="body"/></Td>
		<td><jsp:getProperty name="entry" property="timestamp"/></td>
	</tr>
<% } %>
</table>

</html>
