<%@ page contentType="text/html" import="com.freePowder.beans.*,java.util.*, 
                                         com.freePowder.web.helper.DelegateFactory,
										 com.freePowder.web.helper.ListingDelegate,
										 import com.freePowder.business.ListingServiceException"%>
										 
<% ListingDelegate listingDelegate;
   listingDelegate = DelegateFactory.getInstance().getListingDelegate();
   
   try {
   		List productlist = listingDelegate.getProducts();
		request.setAttribute("productlist", productlist);
   } 
   catch (ListingServiceException e) {
		e.printStackTrace();
   }
   
%>
<jsp:useBean id="entry" class="com.freePowder.beans.BrowseBean"/>
<jsp:useBean id="productlist" class="java.util.ArrayList" scope="request"/>


<!-- Start Browse Section-->
<table width="100%"  border="0" cellspacing="5" cellpadding="0">
  <tr>
    <td width="80"><img src="images/spacer.gif" width="80" height="30"></td>
    <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#EBF1FB">
      <tr>
        <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="183"><img src="images/browse_01.gif" width="183" height="79"></td>
              <td background="images/browse_02.gif"><img src="images/browse_02.gif" width="10" height="79"></td>
              <td width="207"><img src="images/browse_03.jpg" width="207" height="79"></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td>
          <table width="100%"  border="0" cellspacing="3" cellpadding="5">
            <% for (int i = 0; i < productlist.size(); i++) { 
			entry = (BrowseBean) productlist.get(i);
			%>
			<tr>
              <td width="70" rowspan="2" align="center" valign="middle" bgcolor="#FFFFFF"><div align="center"><img src="thumbs/<jsp:getProperty name="entry" property="productname"/>.jpg" width="70" height="70"></div></td>
              <td bgcolor="#FFFFFF"><b><jsp:getProperty name="entry" property="brandname"/></b>, <i><a href="index.jsp?page=product&id=<%=i%>"><jsp:getProperty name="entry" property="productname"/></a></i></td>
              <td width="85" bgcolor="#FFFFFF">$<jsp:getProperty name="entry" property="price"/>.00</td>
            </tr>
			<tr>
              <td bgcolor="#FFFFFF"><jsp:getProperty name="entry" property="cat_lev1"/> :: 
			  <jsp:getProperty name="entry" property="cat_lev2"/> :: 
			  <jsp:getProperty name="entry" property="cat_lev3"/></td>
              <td align="right" bgcolor="#FFFFFF">
			  <form action="/freePowder/" method="post">
				<input name="product_id" type="hidden" id="product_id" value="product_id">
				<input type="submit" value="Add to Cart" class="button-menu">
			  </form></td>
            </tr>
			<% } %>
            <tr bgcolor="#F4BE00">
              <td colspan="3">
                <form name="browse" method="post" action="">
                  <select name="select" class="textfield">
                    <option selected>Brand</option>
                    <option>------------------------------</option>
					<option>Any</option>
                    <option>Salomon</option>
                    <option>Rossignol</option>
                    <option>Sweet</option>
                    <option>K2</option>
                    <option>Oakley</option>
                  </select>
                  <select name="select2" class="textfield">
                    <option selected>Category</option>
                    <option>-------------------------------</option>
                    <option>Any</option>
					<option>Skis</option>
                    <option>Boots</option>
                    <option>Snowboards</option>
                    <option>Accessories</option>
                  </select>
				  <select name="select3" class="textfield">
                    <option selected>Price Range</option>
                    <option>-------------------------------</option>
                    <option>0-100</option>
					<option>101-500</option>
                    <option>501-1000</option>
                    <option>1001-2000</option>
                    <option>2001-3000</option>
					<option>>3000</option>
                  </select>
                  <input type="submit" name="search-button2" value="Search" class="button-menu">
                </form></td>
            </tr>
        </table></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
<!-- End Browse Section-->