/*
 * file:	UserDAOImpl.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	27. Oct 2003
 *
 */

package com.freePowder.dao.support;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.freePowder.beans.UsrBean;
import com.freePowder.common.ServiceLocator;
import com.freePowder.common.ServiceLocatorException;
import com.freePowder.dao.DataAccessException;
import com.freePowder.dao.UserDAO;



public class UserDAOImpl implements UserDAO {
	
	private static ServiceLocator services;
	
	/** Creates a new instance of UserDAOImpl */
	public UserDAOImpl() {
		try {
			services = ServiceLocator.getInstance();
		} catch (ServiceLocatorException e) {
			e.printStackTrace();
		}
	}



	public UsrBean getUser(String loginname, String password) throws DataAccessException {
		Connection con = null;
		try {
			con = services.getConnection();
			PreparedStatement stmt = con.prepareStatement("select first_name, last_name, loginname from usr where loginname = ? and password = ?");
			stmt.setString(1, loginname);
			stmt.setString(2, password);
			ResultSet rs = stmt.executeQuery();
			if (rs.next())
				return createUserBean(rs);
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		return null;
	}
	

	private UsrBean createUserBean(ResultSet rs) throws SQLException {
		UsrBean user = new UsrBean();
		user.setFirst_name(rs.getString("first_name"));
		user.setLast_name(rs.getString("last_name"));
		user.setLoginname(rs.getString("loginname"));
		return user;
	}







	public void addUser(UsrBean bean) throws DataAccessException {
		Connection con = null;
		try {
			con = services.getConnection();
			PreparedStatement stmt = con.prepareStatement("insert into usr (usr_pk, loginname, password, first_name, last_name, email, sex, address, zip) values (usr_seq.nextval, ?, ?, ?, ?, ?, ?, ?, ?)");
			stmt.setString(1, bean.getLoginname());
			stmt.setString(2, bean.getPassword());
			stmt.setString(3, bean.getFirst_name());
			stmt.setString(4, bean.getLast_name());
			stmt.setString(5, bean.getEmail());
			stmt.setString(6, bean.getSex());
			stmt.setString(7, bean.getAddress());
			stmt.setString(8, bean.getZip());
			int n = stmt.executeUpdate();
			if (n != 1)
				throw new DataAccessException("Did not insert one row into database");
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
	}







}
