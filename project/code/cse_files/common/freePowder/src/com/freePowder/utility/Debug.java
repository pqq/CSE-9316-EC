/*
 * file:	Debug.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	04. Nov 2003
 *
 */

package com.freePowder.utility;

import java.util.Date;
import java.io.File;
import java.io.PrintStream;
import java.io.FileOutputStream;
import java.io.IOException;

import com.freePowder.utility.Parser;


public class Debug {
	private boolean debug;
	private boolean timestamp;
	private boolean logtofile;
	private String referer;
	private String outputString;
	private String logfilename;
	private String logfiledir;

	// Constructor
	public Debug(String referer_){
		referer			= referer_;
		outputString	= null;
		debug			= true;
		timestamp		= true;
		logtofile		= true;
		logfilename		= "debug.txt";
		logfiledir		= "/import/cage/1/ecom12/private/common";
	}


	// print out error message
	public void print (String string){
		if (debug){
			String filename;
			
			if (timestamp) {
				String timeStamp = (new Date()).toString();
				timeStamp = Parser.parse(timeStamp, " ")[3];
				outputString = "["+ referer + " "+timeStamp+"]: "+string;
			} else
				outputString = "["+ referer +"]: "+string;
				
			System.out.println(outputString);
			
			if (logtofile){
				File logfile;
				filename = logfiledir+"/"+logfilename;
				logfile = new File(filename);

				if ( !logfile.exists() ){
					try {
						logfile.createNewFile();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				/* 
				 * WRITE TO FILE 
				 */
    			try{
					// Create a file output stream and connect 
					// a print stream to the output stream
					FileOutputStream fos = new FileOutputStream(filename, true);
					PrintStream p = new PrintStream(fos);

					p.println(outputString); //print one line   
					p.close();
					fos.close();
			    }
				catch (IOException ioe) {
					System.err.println ("Error writing to file "+ioe);
				} 



			} // logtofile
			
		} // debug
	} // print
}
