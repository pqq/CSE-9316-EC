/*
 * file:	ErrorCommand.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	25. Oct 2003
 *
 */

package com.freePowder.web;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;




public class ErrorCommand implements Command {
	
	/** Creates a new instance of ErrorCommand */
	public ErrorCommand() {
	}
	
	public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("The page you were looking for was not found. Please watch TV in stead of surfing the net.");
		out.close();
		return null;
	}
	
}
