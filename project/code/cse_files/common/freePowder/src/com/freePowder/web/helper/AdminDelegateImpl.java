/*
 * file:	AdminDelegateImpl.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	08. Nov 2003
 *
 */

package com.freePowder.web.helper;

import com.freePowder.business.AdminService;
import com.freePowder.business.support.AdminServiceImpl;



public class AdminDelegateImpl extends AdminDelegate {

	private static AdminDelegateImpl instance = new AdminDelegateImpl();
	
	private AdminService service;
	
	private AdminDelegateImpl() {
		service = new AdminServiceImpl();
	}
	
	public static AdminDelegate getInstance() {
		return instance;
	}

	protected AdminService getAdminService() {
		return service;
	}

}
