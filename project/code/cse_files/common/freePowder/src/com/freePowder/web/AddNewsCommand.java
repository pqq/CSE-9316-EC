/*
 * file:	AddNewsCommand.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	25. Oct 2003
 *modified:	28. Oct 2003
 */
package com.freePowder.web;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.freePowder.beans.NewsBean;
import com.freePowder.web.helper.DelegateFactory;
import com.freePowder.web.helper.AdminDelegate;
import com.freePowder.business.AdminServiceException;

public class AddNewsCommand implements Command {
	private static AdminDelegate newsDelegate;
	
	public AddNewsCommand(){
	    newsDelegate = DelegateFactory.getInstance().getAdminDelegate();
	}


	public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		NewsBean news = new NewsBean();
		news.setHeading(request.getParameter("heading"));
		news.setTimestamp(request.getParameter("timestamp"));
		news.setBody(request.getParameter("body"));
		
		try {
			newsDelegate.addNews(news);
			return "/index.jsp?page="+request.getParameter("page")+"";
			
		} catch(AdminServiceException ase) {
			//e.printStackTrace();
			return "/index.jsp";
		}
	}
}
