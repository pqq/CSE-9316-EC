/*
 * file:	AdminServiceImpl.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	08. Nov 2003
 *
 */

package com.freePowder.business.support;

import java.util.List;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.FileReader;
import java.sql.ResultSet;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.sql.PreparedStatement;

import com.freePowder.beans.SupplierBean;
import com.freePowder.beans.NewsBean;
import com.freePowder.business.AdminService;
import com.freePowder.business.AdminServiceException;
import com.freePowder.dao.DAOFactory;
import com.freePowder.dao.DataAccessException;
import com.freePowder.dao.SupplierDAO;
import com.freePowder.dao.NewsDAO;
import com.freePowder.common.ServiceLocator;
import com.freePowder.common.ServiceLocatorException;

import com.freePowder.utility.Debug;


public class AdminServiceImpl implements AdminService {

	private SupplierDAO supplierDao;
	private NewsDAO newsDao;
	private Debug d;

	public AdminServiceImpl() {
		super();
		supplierDao = DAOFactory.getInstance().getSupplierDAO();
		newsDao = DAOFactory.getInstance().getNewsDAO();
		d = new Debug("AdminServiceImpl()");
	}


	public List getSuppliers() throws AdminServiceException {
		d.print("getSuppliers()");
		try {
			return supplierDao.getSuppliers();
		} catch (DataAccessException e) {
			throw new AdminServiceException("Unable to find suppliers", e);
		}
	}


	public SupplierBean getSupplier(int supplier_pk) throws AdminServiceException {
		d.print("getSupplier(pk)");
		try {
			return supplierDao.getSupplier(supplier_pk);
		} catch (DataAccessException e) {
			throw new AdminServiceException("Unable to find supplier", e);
		}
	}


	public boolean updateDatabase(String xml_url) throws AdminServiceException {
		d.print("updateDatabase("+xml_url+")");


       try {
			// -------------------------------------------------------------
			// run the unix command wget to download the XML file
			// store the result locally on the OS
			// -------------------------------------------------------------
            String s = null;
            Process p = Runtime.getRuntime().exec("wget -C off -O body.tmp -S "+xml_url+"");
            
            
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

            // read the output from the command            
            //d.print("Standard output of the command:");
            //while ((s = stdInput.readLine()) != null) {
            // System.out.println(s);
            //}

            //// read any errors from the attempted command
            //d.print("Standard error of the command (if any):\n");
            //while ((s = stdError.readLine()) != null) {
            // System.out.println(s);
			// d.print(s);
            //}


			// ---------------------------------------------------------------
			// read the file on the OS where the result is stored
			// ---------------------------------------------------------------
			/* 
				--- --------------- ---
				--- XML feed format ---
				--- --------------- ---
				<brand>
				    <brand_pk>1</brand_pk>
				    <brandname>Rossignol</brandname>
				</brand>
				<product>
				    <product_pk>0</product_pk>
				    <productname>Scream_10</productname>
				    <brand_fk>0</brand_fk>
				    <cat_lev1>Skis</cat_lev1>
				    <cat_lev2>Downhill</cat_lev2>
				    <cat_lev3>Freeride</cat_lev3>
				</product>
				<supplier_product>
				    <supplier_fk>1</supplier_fk>
				    <product_fk>68</product_fk>
				    <price>32000</price>
				</supplier_product>
				--- --------------- ---
			*/

            String sql = "";
			boolean new_table = false;
			boolean string = true;
			boolean deleted = false;
			int i;
            String line;
            BufferedReader reader = new BufferedReader(new FileReader("body.tmp"));

			line = reader.readLine();
			while (line != null){
				string = true;
				
				//d.print("processing (1): "+line);
				line = line.trim();
				d.print("processing (2): "+line);
				
				if( line.equals("<brand>") ){
					
					if (!deleted){
						dodgyInsert("delete from supplier_product");
						dodgyInsert("delete from product");
						dodgyInsert("delete from brand");
						deleted = true;
					}
					
					sql = "insert into brand (brand_pk, brandname) values(";
					new_table = true;
				} else if( line.equals("<product>") ){
					sql = "insert into product (product_pk, productname, brand_fk, cat_lev1, cat_lev2, cat_lev3) values(";
					new_table = true;
				} else if( line.equals("<supplier_product>") ){
					sql = "insert into supplier_product (supplier_fk, product_fk, no_instock, price) values(";
					new_table = true;
				} else if( line.equals("</brand>") || line.equals("</product>") || line.equals("</supplier_product>") ){
					sql = sql.substring(0,sql.length()-1);
					sql = sql + ")";
					dodgyInsert(sql);
					//d.print(sql);
					new_table = false;
					sql = "";
				} else if ( new_table ){
					if ( line.indexOf("pk")>-1 || line.indexOf("fk")>-1 || line.indexOf("price")>-1 || line.indexOf("no_")>-1 ){
						string = false;
					}

					i = line.indexOf(">");
					line = line.substring(i+1, line.length());
					
					i = line.indexOf("<");
					line = line.substring(0, i);
					
					d.print("value: "+line);
					
					if (string){
						sql = sql + "'" + line + "',";
					} else {
						sql = sql + "" + line + ",";
					}
				}
				
				line = reader.readLine();
			}			
			reader.close();

			//d.print(content);


			
        }
        catch (IOException e) {
			d.print("Exception: "+e.getMessage());
            e.printStackTrace();
        }

			
		return true;
		
	}
	
	

	// ---------------------------------------------------------------------------
	// insert into database (this should be done in the DAO layer, not in service...)
	// ---------------------------------------------------------------------------
	private void dodgyInsert(String sql){

		d.print("dodgyInsert("+sql+")");
		
		ServiceLocator services;
		Connection con = null;
		try {
			services = ServiceLocator.getInstance();
			con = services.getConnection();

			PreparedStatement stmt = con.prepareStatement(sql);
			int n = stmt.executeUpdate();
			if (n != 1){
				d.print("dodgyInsert(): result from operation <> 1");
			}

			con.close();
			
			//CallableStatement callableStatement = con.prepareCall("begin ? := freepowder.xml_import; end;");
			//callableStatement.registerOutParameter(1, Types.CHAR);
			//callableStatement.execute();
			//d.print("********** Return value is: " + callableStatement.getString (1));

			// Call a function with an IN parameter
			//    {
			//      CallableStatement funcin =3D conn.prepareCall ("begin ? :=3D =funcin (?); end;");
			//      funcin.registerOutParameter (1, Types.CHAR);
			//      funcin.setString (2, "testing");
			//      funcin.execute ();
			//      System.out.println ("Return value is: " + funcin.getString (1));
			//    }

		} catch (SQLException e){
			d.print("exception: "+e.getMessage());
		} catch (ServiceLocatorException e) {
			d.print("exception: "+e.getMessage());
		}	
	}
	







	public void addNews(NewsBean bean) throws AdminServiceException {
		d.print("addNews()");
		try {
			newsDao.addNews(bean);
		} catch (DataAccessException e) {
			throw new AdminServiceException("Unable to add news", e);
		}
	}





}
