/*
 * file:	Parser.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	04. Nov 2003
 *
 */

package com.freePowder.utility;

import java.util.StringTokenizer;


public class Parser {


    static public String[] parse(String str, String delim){
		String[] tokens = null;
		int i=0;
				
	    StringTokenizer st = new StringTokenizer(str,delim);
		tokens = new String[100];

	    while (st.hasMoreTokens()) {
			tokens[i] = new String(st.nextToken());
			i++;
	    }
		return tokens;
    }



    static public int noTokens(String str, String delim){
		int i=0;
				
	    StringTokenizer st = new StringTokenizer(str,delim);
	    while (st.hasMoreTokens()) {
			st.nextToken();
			i++;
	    }
		return i;
    }

}