/*
 * file:	SupplierDAO.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	08. Nov 2003
 *
 */

package com.freePowder.dao;

import java.util.List;

import com.freePowder.beans.SupplierBean;


public interface SupplierDAO {
	
	List getSuppliers() throws DataAccessException;
	SupplierBean getSupplier(int supplier_pk) throws DataAccessException;
	
}
