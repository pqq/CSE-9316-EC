/*
 * file:	SupplierDAOImpl.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	27. Oct 2003
 *
 */

package com.freePowder.dao.support;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import com.freePowder.beans.SupplierBean;
import com.freePowder.common.ServiceLocator;
import com.freePowder.common.ServiceLocatorException;
import com.freePowder.dao.DataAccessException;
import com.freePowder.dao.SupplierDAO;

import com.freePowder.utility.Debug;



public class SupplierDAOImpl implements SupplierDAO {
	
	private static ServiceLocator services;
	private static Debug d;

	public SupplierDAOImpl() {
		d = new Debug("SupplierDAOImpl()");
		try {
			services = ServiceLocator.getInstance();
		} catch (ServiceLocatorException e) {
			e.printStackTrace();
		}
	}



	public List getSuppliers() throws DataAccessException {
		d.print("getSuppliers()");
		Connection con = null;
		List list = new ArrayList();
		try {
			con = services.getConnection();
			PreparedStatement stmt = con.prepareStatement("select suppliername, xml_url, update_url from supplier");
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				d.print("rs.next()");
				list.add(createSupplierBean(rs));
			}
		} catch (ServiceLocatorException e) {
			d.print("Unable to retrieve connection");
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			d.print("Unable to execute query");
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		return list;
	}







	public SupplierBean getSupplier(int supplier_pk) throws DataAccessException {
		Connection con = null;
		try {
			con = services.getConnection();
			PreparedStatement stmt = con.prepareStatement("select suppliername, xml_url, update_url from supplier where supplier_pk = ?");
			stmt.setInt(1, supplier_pk);
			ResultSet rs = stmt.executeQuery();
			if (rs.next())
				return createSupplierBean(rs);
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		return null;
	}




	

	private SupplierBean createSupplierBean(ResultSet rs) throws SQLException {
		SupplierBean bean = new SupplierBean();
		bean.setSuppliername(rs.getString("suppliername"));
		bean.setXml_url(rs.getString("xml_url"));
		bean.setUpdate_url(rs.getString("update_url"));
		return bean;
	}


}
