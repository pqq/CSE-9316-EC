/*
 * file:	ListingService.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	07. Nov 2003
 *
 */

package com.freePowder.business;

import java.util.List;




public interface ListingService {

	List getNews() throws ListingServiceException;
	List getWeather() throws ListingServiceException;
	List getProducts() throws ListingServiceException;
}
