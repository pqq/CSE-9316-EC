/*
 * file:	SupplierBean.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	08. Nov 2003
 *
 */

package com.freePowder.beans;

import java.io.Serializable;



public class SupplierBean implements Serializable {

	private int supplier_pk;
	private String suppliername;
	private String xml_url;
	private String update_url;
	private String last_update;


	public SupplierBean() {
	}
	
	public int getSupplier_pk() {
		return supplier_pk;
	}
	
	public void setSupplier_pk(int supplier_pk) {
		this.supplier_pk = supplier_pk;
	}
	
	public String getSuppliername() {
		return suppliername;
	}
	
	public void setSuppliername(String suppliername) {
		this.suppliername = suppliername;
	}
	
	public String getXml_url() {
		return xml_url;
	}

	public void setXml_url(String xml_url){
		this.xml_url = xml_url;
	}

	public String getUpdate_url() {
		return update_url;
	}

	public void setUpdate_url(String update_url){
		this.update_url = update_url;
	}

	public String getLast_update() {
		return last_update;
	}

	public void setLast_update(String last_update){
		this.last_update = last_update;
	}
	
	public String toString() {
		return "[SUPPLIER]" + suppliername + " " + xml_url + " " + update_url+ " " + last_update;
	}
}
