/*
 * file:	AdminDelegate.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	08. Nov 2003
 */

package com.freePowder.web.helper;

import java.util.List;

import com.freePowder.beans.SupplierBean;
import com.freePowder.beans.NewsBean;
import com.freePowder.business.AdminService;
import com.freePowder.business.AdminServiceException;

import com.freePowder.utility.Debug;


public abstract class AdminDelegate {

	protected abstract AdminService getAdminService();
	protected Debug d;

	public List getSuppliers() throws AdminServiceException {
		d = new Debug("AdminDelegate()");
		d.print("getSuppliers()");
		return getAdminService().getSuppliers();
	}

	public SupplierBean getSupplier(int supplier_pk) throws AdminServiceException {
		d = new Debug("AdminDelegate()");
		d.print("getSupplier()");
		return getAdminService().getSupplier(supplier_pk);
	}

	public boolean updateDatabase(String xml_url) throws AdminServiceException {
		d = new Debug("AdminDelegate()");
		d.print("updateDatabase("+xml_url+")");
		
		return getAdminService().updateDatabase(xml_url);
	}
	
	public void addNews(NewsBean bean) throws AdminServiceException {
	    d = new Debug("AdminDelegate()");
	    d.print("addNews()");
	    
	    getAdminService().addNews(bean);
	}
}
