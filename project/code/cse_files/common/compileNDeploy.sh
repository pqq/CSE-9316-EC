clear
echo "*** Compiling and deploying freePowder ***"

# ------------------------
# cleaning by deleting existing class files
# ------------------------
echo "*** Deleting existing .class files ***"
.clean.sh

# ------------------------
# changing to right directory
# ------------------------
cd /home/ecom12/private/common/freePowder

# ------------------------
# setting classpaths
# ------------------------
export CLASSPATH='.'
source /home/ecom12/bea/user_projects/freePowder/setEnv.sh

echo "*** Classpath is set ***"
#echo CLASSPATH=${CLASSPATH}

# ------------------------
# copying files into build directory
# ------------------------
cp -r web/* build/

# ------------------------
# compiling .java
# ------------------------
cd src
javac -d ../build/WEB-INF/classes com/freePowder/web/*.java
javac -d ../build/WEB-INF/classes com/freePowder/common/*.java
javac -d ../build/WEB-INF/classes com/freePowder/beans/*.java
javac -d ../build/WEB-INF/classes com/freePowder/dao/support/*.java
javac -d ../build/WEB-INF/classes com/freePowder/dao/*.java
javac -d ../build/WEB-INF/classes com/freePowder/utility/*.java

echo "*** Javafiles are compiled ***"

# ---------------------------
# deplyoing the application
# ---------------------------
cd ../build
jar -cf freePowder.war *
cp freePowder.war /home/ecom12/bea/user_projects/freePowder/applications/

echo "*** The application is deployed ***"


# ------------------------
# give access on all files to all users
# ------------------------
echo "*** chmod g+rwx on all files ***"
cd ~ecom12/private/common/
.changemode.sh

