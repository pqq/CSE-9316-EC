/*
 * RequestDispatcher.java
 *
 * Created on 9 August 2003, 10:58
 */

package com.myPhonebook.web;

import java.io.*;
import java.net.*;

import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

/**
 *
 * @author  yunki
 * @version
 */
public class ControllerServlet extends HttpServlet {
	
	private Map commands;
	
	/** Initializes the servlet.
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		commands = new HashMap();
		commands.put("add", new AddCommand());
		commands.put("delete", new DeleteCommand());
		commands.put("list", new ListCommand());
		commands.put("login", new LoginCommand());
		commands.put("logout", new LogoutCommand());        
		commands.put("PAGE_NOT_FOUND", new ErrorCommand());
	}
	
	/** Destroys the servlet.
	 */
	public void destroy() {
		
	}
	
	/** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
	 * @param request servlet request
	 * @param response servlet response
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		Command cmd = (Command) commands.get(request.getParameter("operation"));
		if (cmd == null) {
			cmd = (Command) commands.get("PAGE_NOT_FOUND");
		}
		String next = cmd.execute(request, response);
	}
	
	/** Handles the HTTP <code>GET</code> method.
	 * @param request servlet request
	 * @param response servlet response
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		processRequest(request, response);
	}
	
	/** Handles the HTTP <code>POST</code> method.
	 * @param request servlet request
	 * @param response servlet response
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		processRequest(request, response);
	}
	
	/** Returns a short description of the servlet.
	 */
	public String getServletInfo() {
		return "Short description";
	}
	
}
