/*
 * LoginCommand.java
 *
 * Created on 9 August 2003, 11:12
 */

package com.myPhonebook.web;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This is the command that will be used for logging in users.
 * If logon is successful, the command should place a list of phonebook entries
 * in the request attriubutes.
 * @author  yunki
 */
public class LoginCommand implements Command {
	
	/** Creates a new instance of LoginCommand */
	public LoginCommand() {
	}
	
	public String execute(HttpServletRequest request, HttpServletResponse response) 
	throws ServletException, IOException {
	
		//TODO: set the response content type to "text/html" here
		//TODO: You'll have to use getWriter() to output some HTML texts 
		//TODO: close the output stream
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("The operation login was requested");
		out.close();
		return null;
	}
	
}
