% more compileNDeploy.sh
clear
echo "*** Compiling and deploying myPhonebook ***"

# ------------------------
# changing to right directory
# ------------------------
cd /home/ecom12/private/common/myPhonebook/

# ------------------------
# setting classpaths
# ------------------------
export CLASSPATH='.'
source /home/ecom12/bea/user_projects/freePowder/setEnv.sh

echo "*** Classpath is set ***"
#echo CLASSPATH=${CLASSPATH}

# ------------------------
# copying files into build directory
# ------------------------
cp web/*.jsp build/
cp web/WEB-INF/web.xml build/WEB-INF/

# ------------------------
# compiling .java
# ------------------------
cd src
javac -d ../build/WEB-INF/classes/ com/myPhonebook/web/*.java
#javac -d ../build/WEB-INF/classes/ com/myPhonebook/common/*.java

echo "*** Javafiles are compiled ***"

# ---------------------------
# deploying the application
# ---------------------------
cd ../build
jar -cf myPhonebook.war *
cp myPhonebook.war /home/ecom12/bea/user_projects/freePowder/applications/

echo "*** The application is deployed ***"
