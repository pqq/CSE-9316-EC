/*
 * ServiceLocator.java
 * Created on 10/08/2003
 *
 */
package com.myPhonebook.common;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * @author $author
 */
public class ServiceLocator
{

	private static ServiceLocator instance;

	private Properties env;
	
	private DataSource ds;

	public static ServiceLocator getInstance() throws ServiceLocatorException
	{
		if (instance == null)
			instance = new ServiceLocator();
		return instance;
	}

	private ServiceLocator() throws ServiceLocatorException
	{
		env = new Properties();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
	//	env.put(Context.PROVIDER_URL, "t3://localhost:7003");
	//	env.put(Context.SECURITY_PRINCIPAL, "jazzman");//have to change the user
	//	env.put(Context.SECURITY_CREDENTIALS, "classact7");//have to change the password
	
		env.put(Context.PROVIDER_URL, "weill.cse.unsw.edu.au:9018");
		env.put(Context.SECURITY_PRINCIPAL, "fp");//have to change the user
		env.put(Context.SECURITY_CREDENTIALS, "freePowder8");//have to change the password
		try
		{
			ds = getDataSource();
			//getDataSource() is implemented by the servicelocator class itself
		}
		catch (ServiceLocatorException e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Finds a data source by looking up the initial context
	 * @return
	 * @throws ServiceLocatorException
	 */
	public DataSource getDataSource() throws ServiceLocatorException
	{
		if (ds == null)
		{
			try
			{
				InitialContext ctx = new InitialContext(env);
				ds = (DataSource)ctx.lookup("fpDataSource");

			}
			catch (NamingException e)
			{
				throw new ServiceLocatorException("Unable to locate data source; " + e.getMessage(), e);
			}
		}
		return ds;
	}
	
	/**
	 * @return
	 * @throws ServiceLocatorException
	 */
	public Connection getConnection() throws ServiceLocatorException
	{
			try
			{
				return getDataSource().getConnection();
			}
			catch (SQLException e)
			{
				throw new ServiceLocatorException("Unable to open connection to data source; " + e.getMessage(), e);			
			}
	}
}