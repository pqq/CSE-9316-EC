/*
 * NewsBean.java
 *
 * Created on 21 August 2003, 10:59 pm
 */
package com.myPhonebook.beans;

import java.io.Serializable;
/**
 * Class that represents news for the system
 *
 * @author  $Nosh
 */
public class NewsBean implements Serializable
{
	/**
	 * The news primary-key in the database
	 */
	private int news_pk;
	
	/**
	 * The the heading for the news
	 */
	private String heading;
	
	/**
	 * The body of the news
	 */
	private String body;
	

	/** Creates a new instance of News */
	public NewsBean()
	{
	}
	/** Getter for news primary key.
	 * @return Value of primary key for news table that is an integer.
	 *
	 */
	public int newsPk()
	{
		return news_pk;
	}
	/** Setter for news primary key.
	 * @param news_pk new Value of primary key for news table
	 * that is an integer.
	 *
	 */
	public void setNewsPk(int news_pk)
	{
		this.news_pk = news_pk;
	}
	/** Getter for property firstname.
	 * @return Value of property firstname.
	 *
	 */
	public java.lang.String getHeading()
	{
		return heading;
	}
	/** Setter for property firstname.
	 * @param firstname New value of property firstname.
	 *
	 */
	public void setHeading(java.lang.String heading)
	{
		this.heading = heading;
	}
	/** Getter for property lastname.
	 * @return Value of property lastname.
	 *
	 */
	public java.lang.String getBody()
	{
		return body;
	}
	/** Setter for property lastname.
	 * @param lastname New value of property lastname.
	 *
	 */
	public void setBody(java.lang.String body)
	{
		this.body = body;
	}
	/** Getter for property accessLevel.
	 * @return Value of property accessLevel.
	 *
	 */
	
	public String toString()
	{
		return "[NEWS]" + heading + " (" + body + ") #" + hashCode();
	}
}