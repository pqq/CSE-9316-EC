/*
 * NewsDAOImpl.java
 *
 * Created on 9 August 2003, 14:41
 */
package com.myPhonebook.dao.support;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.myPhonebook.beans.NewsBean;
import com.myPhonebook.common.ServiceLocator;
import com.myPhonebook.common.ServiceLocatorException;
import com.myPhonebook.dao.DataAccessException;
import com.myPhonebook.dao.NewsDAO;
/**
 * The Data Access Object for users.
 *
 * @author  nosh
 */
public class NewsDAOImpl implements NewsDAO
{
	private static ServiceLocator services;
	/** Creates a new instance of UserDAOImpl */
	public NewsDAOImpl()
	{
		try
		{
			services = ServiceLocator.getInstance();
		}
		catch (ServiceLocatorException e)
		{
			e.printStackTrace();
		}
	}
	/**
	 * @see com.myPhonebook.dao.UserDAO#findByLoginDetails(java.lang.String, java.lang.String)
	 */
	public NewsBean findByNewsPk(int news_pk) throws DataAccessException
	{
		Connection con = null;
		try 
		{
			con = services.getConnection();
			PreparedStatement stmt = con.prepareStatement("select * from news where news_pk = ?");
			stmt.setString(1, news_pk);
			//stmt.setString(2, password);
			ResultSet rs = stmt.executeQuery();
			if (rs.next())
				return createNewsBean(rs);
		}
		catch (ServiceLocatorException e)
		{
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		}
		catch (SQLException e)
		{
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		}
		finally
		{
			if (con != null)
			{
				try
				{
					con.close();
				}
				catch (SQLException e1)
				{
					e1.printStackTrace();
				}
			}
		}
		return null;
	}
	/**
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private NewsBean createNewsBean(ResultSet rs) throws SQLException
	{
		NewsBean news = new NewsBean();
		news.setNewsPk(rs.getInt("news_pk"));
		news.setHeading(rs.getString("heading"));
		news.setBody(rs.getString("body"));
		return news;
	}
}