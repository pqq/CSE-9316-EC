/*
 * NewsDAO.java
 *
 * Created on 9 August 2003, 14:39
 */
package com.myPhonebook.dao;

import com.myPhonebook.beans.UserBean;
/**
 *
 * @author Nosh
 */
public interface NewsDAO 
{	
	/**
	 * Tries to locate a news item with the given primary-key.
	 * 
	 * @param username The username to use to find the user
	 * @param password The password to use to find the user
	 * @return The UserBean if there is a user with the username and password,
	 * null otherwise.
	 * @throws DataAccessException If error occurs while connecting and retrieving
	 * the database
	 */
	NewsBean findByNewsPk(int news_pk) throws DataAccessException;	
}