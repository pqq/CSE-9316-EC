<%@ page contentType="text/html" import="com.freePowder.beans.*,java.util.*"%>
<jsp:useBean id="user" type="com.freePowder.beans.UsrBean" scope="session"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Login Success</title>
  </head>
  
  <body>
	<h2>Welcome <jsp:getProperty name="user" property="first_name"/> <jsp:getProperty name="user" property="last_name"/></h2>
	<font color="blue">User logged in...</font>	
	<p>
	<a href="index.jsp">Login again</a> (logout link, to come...)
  </body>
</html>
