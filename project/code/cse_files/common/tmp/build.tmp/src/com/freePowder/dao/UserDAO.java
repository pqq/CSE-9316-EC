/*
 * file:	UserDAO.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	27. Oct 2003
 *
 */

package com.freePowder.dao;

import com.freePowder.beans.UsrBean;


public interface UserDAO {
	
	UsrBean getUser(String loginname, String password) throws DataAccessException;
	
}
