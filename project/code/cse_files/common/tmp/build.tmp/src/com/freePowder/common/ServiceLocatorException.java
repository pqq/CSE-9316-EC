/*
 * file:	ServiceLocatorException.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	25. Oct 2003
 *
 */

package com.freePowder.common;


public class ServiceLocatorException extends NestedException {
	
	public ServiceLocatorException(String message) {
		super(message);
	}


	public ServiceLocatorException(String message, Throwable cause) {
		super(message, cause);
	}

}
