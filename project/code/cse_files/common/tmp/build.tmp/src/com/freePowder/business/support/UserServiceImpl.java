/*
 * file:	UserServiceImpl.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	02. Nov 2003
 *
 */

package com.freePowder.business.support;

import java.util.List;

import com.freePowder.beans.UsrBean;
import com.freePowder.business.UserService;
import com.freePowder.business.UserServiceException;
import com.freePowder.business.UserLoginFailedException;
import com.freePowder.dao.DAOFactory;
import com.freePowder.dao.DataAccessException;
import com.freePowder.dao.UserDAO;



public class UserServiceImpl implements UserService {


	private UserDAO userDao;

	/**
	 * 
	 */
	public UserServiceImpl() {
		super();
		userDao = DAOFactory.getInstance().getUserDAO();
	}

	public UsrBean login(String loginname, String password) throws UserLoginFailedException {
		UsrBean user = null; 
		try {
			user = userDao.getUser(loginname, password);
			if (user == null)
				throw new UserLoginFailedException("Login failed for " + loginname);
		} catch (DataAccessException e) {
			throw new UserLoginFailedException("Unable to login", e);
		}
		return user;

	}

}
