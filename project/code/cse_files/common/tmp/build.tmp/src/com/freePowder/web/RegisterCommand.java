/*
 * file:	RegisterCommand.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	25. Oct 2003
 *
 */

package com.freePowder.web;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.freePowder.beans.UsrBean;
import com.freePowder.web.helper.DelegateFactory;
import com.freePowder.web.helper.UserDelegate;
import com.freePowder.business.UserServiceException;






public class RegisterCommand implements Command {

	private static UserDelegate userDelegate;	
	
	public RegisterCommand() {
		userDelegate = DelegateFactory.getInstance().getUserDelegate();
	}
	
	public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("DEBUG: RegisterCommand.execute()");
		
		// get the request parameters
		HttpSession session = request.getSession(false);
		UsrBean u = new UsrBean();
		u.setLoginname(request.getParameter("loginname"));
		u.setFirst_name(request.getParameter("first_name"));
		u.setLast_name(request.getParameter("last_name"));
		u.setEmail(request.getParameter("email"));
		
		//try {
			//userDelegate.addUser(u);
			return "/index.jsp";
		//} catch (UserServiceException e) {
		//	e.printStackTrace();
		//}
		//return "/error.jsp";
	}
}
                                                                                                             