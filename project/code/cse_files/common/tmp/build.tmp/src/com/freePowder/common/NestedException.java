/*
 * file:	NestedException.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	25. Oct 2003
 *
 */

package com.freePowder.common;


public class NestedException extends Exception {

	private Throwable cause;


	public NestedException(String message) {
		super(message);
	}


	public NestedException(String message, Throwable cause) {
		super(message);
		this.cause = cause;
	}


	public NestedException(Throwable cause) {
		this.cause = cause;
	}

	
	public void printStackTrace() {
		System.err.println("Nested exception is " + cause.getMessage());
		super.printStackTrace();
	}

}
