/*
 * file:	UserDelegate.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	02. Nov 2003
 *
 */
package com.freePowder.web.helper;

import java.util.List;

import com.freePowder.beans.UsrBean;
import com.freePowder.business.UserService;
import com.freePowder.business.UserLoginFailedException;



public abstract class UserDelegate {

	protected abstract UserService getUserService();

//	private PhonebookService phonebookService;
	
	/**
	 * Finds a user with the given details
	 * @param username The username to login with
	 * @param password The password to login with
	 * @return A user if one exists with the given details
	 * @throws UserLoginFailedException When no such user is found
	 */
	public UsrBean login(String loginname, String password) throws UserLoginFailedException {
		System.out.println("DEBUG: UserDelegate.login("+loginname+","+password+")");
		return getUserService().login(loginname, password);
	}
	
	/**
	 * Returns all the phonebooks for a particular user
	 * @param userId
	 * @return A list of all phonebook records
	 * @throws PhonebookServiceException When an error occurs
	 */
//	public List getPhonebookRecords(int userId) throws PhonebookServiceException {
//		return getPhonebookService().getPhonebookRecords(userId);
//	}
	/**
	 * Adds a record to the phonebook
	 * @param bean The phone book record to add
	 * @throws PhonebookServiceException When an error occurs
	 */
//	public void addRecord(PhonebookBean bean) throws PhonebookServiceException {
//		getPhonebookService().addRecord(bean);
//	}
	/**
	 * Removes a phonebook record from the database
	 * @param id The id of the record to delete
	 * @throws PhonebookServiceException When an error occurs
	 */
//	public PhonebookBean deleteRecord(int id) throws PhonebookServiceException {
//		return getPhonebookService().deleteRecord(id);
//	}

}
