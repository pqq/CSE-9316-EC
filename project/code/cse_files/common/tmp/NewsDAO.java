/*
 * file:	NewsDAO.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	07. Nov 2003
 *
 */

package com.freePowder.dao;

import java.util.List;

import com.freePowder.beans.NewsBean;


public interface NewsDAO {

	List getNews() throws DataAccessException;

}
