/*
 * file:	ListNewsCommand.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	08. Nov 2003
 *
 */

package com.freePowder.web;

import java.io.*;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.freePowder.beans.NewsBean;
import com.freePowder.web.helper.DelegateFactory;
import com.freePowder.web.helper.NewsDelegate;
import com.freePowder.business.ListingServiceException;

import com.freePowder.utility.Debug;



public class ListNewsCommand implements Command {

	private static NewsDelegate newsDelegate;
	private static Debug d;
	
	public ListNewsCommand() {
		d = new Debug("ListNewsCommand()");
		d.print("Constructor()");
		newsDelegate = DelegateFactory.getInstance().getNewsDelegate();
	}
	
	public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		d.print("execute()");
		
		HttpSession session = request.getSession();
		
		try {
			List list = newsDelegate.getNews();
			request.setAttribute("newslist", list);
						
			return "/testnews.jsp";
		} catch (ListingServiceException e) {
			e.printStackTrace();
		}	
		return "/index.jsp";
	}
}
