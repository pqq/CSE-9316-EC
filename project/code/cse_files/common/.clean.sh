rm -rfv freePowder/*.class

# the first command don't work, so I do everything manually
rm -rf freePowder/build/WEB-INF/classes/com/freePowder/beans/*.class
rm -rf freePowder/build/WEB-INF/classes/com/freePowder/business/*.class
rm -rf freePowder/build/WEB-INF/classes/com/freePowder/common/*.class
rm -rf freePowder/build/WEB-INF/classes/com/freePowder/dao/*.class
rm -rf freePowder/build/WEB-INF/classes/com/freePowder/utility/*.class
rm -rf freePowder/build/WEB-INF/classes/com/freePowder/web/*.class
