#!/usr/local/bin/perl

# -----------------------------------------------------
# Filename:		backup.pl
# Author:		Frode Klevstul (frode@klevstul.com)
# Started:		02.11.03
# -----------------------------------------------------

# -------------------
# use
# -------------------
use strict;

# -------------------
# global declarations
# -------------------
my $backupdir = "backup";
my $folder2b = "freePowder";


# -------------------
# main
# -------------------
&backup_folder;

# -------------------
# sub procedures
# -------------------
sub backup_folder{
	my $backuptime = &timestamp;
	
	system("cp -R $folder2b $backupdir/$folder2b\_$backuptime");
	print "'$folder2b' backed up as '$backupdir/$folder2b\_$backuptime'\n";
}


sub timestamp{
	my $time = time;
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($time);
	my $timestamp;
	
	$year = $year+1900;
	$mon = $mon+1;

	$sec =~ s/^(\d){1}$/0$1/;
	$min =~ s/^(\d){1}$/0$1/;
	$hour =~ s/^(\d){1}$/0$1/;
	$mday =~ s/^(\d){1}$/0$1/;
	$mon =~ s/^(\d){1}$/0$1/;

	$timestamp = $year . $mon . $mday . "_" . $hour . $min . $sec;	

	return $timestamp;
}

