-- -------------------------------------------------------
-- set variables
-- -------------------------------------------------------
set define off


-- -------------------------------------------------------
-- prompt information
-- -------------------------------------------------------
prompt   + ---------------------------------------------- +
prompt   | package: freepowder                            |
prompt   | desc:    ..................................... |
prompt   | author:  frode klevstul (frode@klevstul.com)   |
prompt   | started: 10.11.2003                            |
prompt   | build:   20031011                              |
prompt   + ---------------------------------------------- +


-- -------------------------------------------------------
-- package header
-- -------------------------------------------------------

create or replace package freepowder is

	function xml_import
		(
			p_string	in varchar2		default null
		) return varchar2;

end;
/
show errors;



-- -------------------------------------------------------
-- package body
-- -------------------------------------------------------
create or replace package body freepowder is


---------------------------------------------------------
-- name:        xml_import
-- what:        imports xml into the database
-- author:      frode klevstul
-- start date:  10.11.2003
---------------------------------------------------------
function xml_import
		(
			p_string  in varchar2 default null
		) return varchar2
is
        v_return	varchar2(100)	default		null;
begin

		v_return := 'eCom';
        return v_return;

end xml_import;


-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- ends package body
/
show errors;

