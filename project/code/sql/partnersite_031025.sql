/*==============================================================*/
/* DBMS name:      ORACLE Version 8i2 (8.1.6)                   */
/* Created on:     25/10/2003 2:43:04 PM                        */
/*==============================================================*/


alter table PRODUCT
   drop constraint FK_PRODUCT_REFERENCE_BRAND;

alter table PRODUCT_SPECIFICATION
   drop constraint FK_PRODUCT__REFERENCE_PRODUCT;

alter table PRODUCT_SPECIFICATION
   drop constraint FK_PRODUCT__REFERENCE_SPECIFIC;

alter table SUPPLIER_PRODUCT
   drop constraint FK_SUPPLIER_REFERENCE_PRODUCT;

alter table SUPPLIER_PRODUCT
   drop constraint FK_SUPPLIER_REFERENCE_SUPPLIER;

drop table BRAND cascade constraints;

drop table ORDERLOG cascade constraints;

drop table PRODUCT cascade constraints;

drop table PRODUCT_SPECIFICATION cascade constraints;

drop table SPECIFICATION cascade constraints;

drop table SUPPLIER cascade constraints;

drop table SUPPLIER_PRODUCT cascade constraints;

/*==============================================================*/
/* Table: BRAND                                                 */
/*==============================================================*/
create table BRAND  (
   BRAND_PK             INT                             not null,
   BRANDNAME            VARCHAR2(64),
   constraint PK_BRAND primary key (BRAND_PK)
);

/*==============================================================*/
/* Table: ORDERLOG                                              */
/*==============================================================*/
create table ORDERLOG  (
   ORDERLOG_PK          int                             not null,
   PARTNER              VARCHAR2(64),
   PRODUCT_PK           INT,
   PRODUCTNAME          VARCHAR2(64),
   AMOUNT               INT,
   TIMESTAMP            DATE,
   constraint PK_ORDERLOG primary key (ORDERLOG_PK)
);

/*==============================================================*/
/* Table: PRODUCT                                               */
/*==============================================================*/
create table PRODUCT  (
   PRODUCT_PK           INT                             not null,
   PRODUCTNAME          VARCHAR2(64),
   BRAND_FK             INT,
   CAT_LEV1             VARCHAR2(64),
   CAT_LEV2             VARCHAR2(64),
   CAT_LEV3             VARCHAR2(64),
   constraint PK_PRODUCT primary key (PRODUCT_PK)
);

/*==============================================================*/
/* Table: PRODUCT_SPECIFICATION                                 */
/*==============================================================*/
create table PRODUCT_SPECIFICATION  (
   PRODUCT_FK           INT                             not null,
   SPECIFICATION_FK     INT                             not null,
   VALUE                VARCHAR2(64),
   constraint PK_PRODUCT_SPECIFICATION primary key (PRODUCT_FK, SPECIFICATION_FK)
);

/*==============================================================*/
/* Table: SPECIFICATION                                         */
/*==============================================================*/
create table SPECIFICATION  (
   SPECIFICATION_PK     INT                             not null,
   NAME                 VARCHAR2(64),
   SUFFIX               VARCHAR2(3),
   constraint PK_SPECIFICATION primary key (SPECIFICATION_PK)
);

/*==============================================================*/
/* Table: SUPPLIER                                              */
/*==============================================================*/
create table SUPPLIER  (
   SUPPLIER_PK          INT                             not null,
   SUPPLIERNAME         VARCHAR2(32),
   constraint PK_SUPPLIER primary key (SUPPLIER_PK)
);

/*==============================================================*/
/* Table: SUPPLIER_PRODUCT                                      */
/*==============================================================*/
create table SUPPLIER_PRODUCT  (
   SUPPLIER_FK          INT                             not null,
   PRODUCT_FK           INT                             not null,
   NO_INSTOCK           INT,
   PRICE                INT,
   constraint PK_SUPPLIER_PRODUCT primary key (SUPPLIER_FK, PRODUCT_FK)
);

alter table PRODUCT
   add constraint FK_PRODUCT_REFERENCE_BRAND foreign key (BRAND_FK)
      references BRAND (BRAND_PK);

alter table PRODUCT_SPECIFICATION
   add constraint FK_PRODUCT__REFERENCE_PRODUCT foreign key (PRODUCT_FK)
      references PRODUCT (PRODUCT_PK);

alter table PRODUCT_SPECIFICATION
   add constraint FK_PRODUCT__REFERENCE_SPECIFIC foreign key (SPECIFICATION_FK)
      references SPECIFICATION (SPECIFICATION_PK);

alter table SUPPLIER_PRODUCT
   add constraint FK_SUPPLIER_REFERENCE_PRODUCT foreign key (PRODUCT_FK)
      references PRODUCT (PRODUCT_PK);

alter table SUPPLIER_PRODUCT
   add constraint FK_SUPPLIER_REFERENCE_SUPPLIER foreign key (SUPPLIER_FK)
      references SUPPLIER (SUPPLIER_PK);

